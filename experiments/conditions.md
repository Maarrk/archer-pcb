# Calibration

- 2020-01-16, 27 deg. C
    - v-batt
    - i-tail1
    - i-tail2
    - i-main

- 2020-10-02
    - all current channels
    - measurement at battery output
    - board passively consumes 65.8 mA

# Mavlink logs

- 2020-01-23
    - chudoby_powerboard.bin - binary dump of bytes received from powerboard through RFD868; Realterm added UNIX timestamp
