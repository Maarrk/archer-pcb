#include "utility.h"
#include "globals.h"
#include "parameters.h"

#include "../../mavlink/include/archer/mavlink.h"

void calculate_motor_speed()
{
  if(main_pulse_count != 0 || (tail1_pulse_count != 0 || tail2_pulse_count != 0)) {
    state_last_rpm_time = get_time_boot();
  }

  main_hertz = (uint16_t)((main_pulse_count * PERIOD_SCALE) / PERIOD / MAIN_POLE_COUNT);
  tail1_hertz = (uint16_t)((tail1_pulse_count * PERIOD_SCALE) / PERIOD / TAIL1_POLE_COUNT);
  tail2_hertz = (uint16_t)((tail2_pulse_count * PERIOD_SCALE) / PERIOD / TAIL2_POLE_COUNT);

  main_pulse_count = 0;
  tail1_pulse_count = 0;
  tail2_pulse_count = 0;
}

uint32_t get_time_boot()
{
  uint32_t retval = ((uint32_t)time_boot_htim) << 16;
  retval += time_boot_htim->Instance->CNT;
  return retval;
}

bool is_recent(uint32_t state_last_time){
  if(state_last_time != UINT32_MAX) {
    if (state_last_time + STATE_TIMEOUT >= get_time_boot()){
      return true;
    }
  }

  return false;
}

void handle_mavlink_char(uint8_t received_char, mavlink_message_t* received_msg) {
  if (mavlink_parse_char(MAVLINK_COMM_0, received_char, received_msg, &received_status)){
    switch(received_msg->msgid){

    case MAVLINK_MSG_ID_ARCHER_BATTERY:
      state_last_batt_time = get_time_boot();

      mavlink_msg_archer_battery_decode(received_msg, &batt_msg);
      batt_voltage = batt_msg.voltage;
      batt_current_main = batt_msg.current_motor_main;
      batt_current_tail1 = batt_msg.current_motor_tail1;
      batt_current_tail2 = batt_msg.current_motor_tail2;
      batt_current_servo = batt_msg.current_servo;
      batt_current_avio = batt_msg.current_avio;

      sent_msg_len = mavlink_msg_to_send_buffer(sent_msg_buf, received_msg);
      HAL_UART_Transmit(companion_computer_huart, sent_msg_buf, sent_msg_len, 100);

      handle_dac_pix();

      break;

    case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
    	sent_msg_len = 0;
    	for(uint16_t i=0; i<PARAM_COUNT; i++) {
    		add_msg_param_value(sent_msg_buf, &sent_msg_len, i);
    	}
    	HAL_UART_Transmit(companion_computer_huart, sent_msg_buf, sent_msg_len, 100);
    	break;

    case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
    	sent_msg_len = 0;

    	mavlink_param_request_read_t request_msg;
    	mavlink_msg_param_request_read_decode(received_msg, &request_msg);
    	if(request_msg.param_index == -1) { // Ignore index, search by id
    		int16_t index = 0;
    		param_t* param;
    		findParam(request_msg.param_id, &index, &param);
    		if(index != -1 && param != 0) {
    			add_msg_param_value(sent_msg_buf, &sent_msg_len, index);
    		}
    	} else {
    		if (request_msg.param_index >= 0 && request_msg.param_index < PARAM_COUNT) {
    			add_msg_param_value(sent_msg_buf, &sent_msg_len, request_msg.param_index);
    		}
    	}

    	if(sent_msg_len > 0) {
    		HAL_UART_Transmit(companion_computer_huart, sent_msg_buf, sent_msg_len, 100);
    	}
    	break;

    case MAVLINK_MSG_ID_PARAM_SET:
    	sent_msg_len = 0;

		mavlink_param_set_t set_msg;
		mavlink_msg_param_set_decode(received_msg, &set_msg);
		int16_t index = 0;
		param_t* param;
		findParam(set_msg.param_id, &index, &param);
		if(index != -1 && param != 0) {
			if(param->type == set_msg.param_type) {
				memcpy(&(param->value.bytes), &(set_msg.param_value), sizeof(float));
			}

			add_msg_param_value(sent_msg_buf, &sent_msg_len, index);
		}
		if(sent_msg_len > 0) {
			HAL_UART_Transmit(companion_computer_huart, sent_msg_buf, sent_msg_len, 100);
		}
		break;
    }
  }
}

mavlink_message_t create_msg_heartbeat() {
  mavlink_message_t msg;
  uint16_t status = ARCHER_STATE_MAINBOARD_OK;
  if (is_recent(state_last_powerboard_time)) status |= ARCHER_STATE_POWERBOARD_OK;
  if (is_recent(state_last_batt_time)) status |= ARCHER_STATE_MEASURING_BATTERY;
  if (is_recent(state_last_rpm_time)) status |= ARCHER_STATE_MEASURING_RPM;
  if (is_recent(state_last_temp_time)) status |= ARCHER_STATE_MEASURING_TEMPERATURE;
  if (is_recent(state_last_angle_time)) status |= ARCHER_STATE_MEASURING_ANGLE;
  if (is_recent(state_last_aero_time)) status |= ARCHER_STATE_MEASURING_AERODYNAMIC;
  if (is_recent(state_last_syncpwm_time)) status |= ARCHER_STATE_SYNC_PWM;

  mavlink_msg_archer_heartbeat_pack(1, 158, &msg, get_time_boot(), status, syncpwm_value);
  return msg;
}

void handle_dac_pix() {
  uint16_t current_value = ((batt_current_main + batt_current_tail1 + batt_current_tail2 +
                             batt_current_servo + batt_current_avio) * DAC_MAX_VAL) / 330 / PIX_AMPERES_PER_VOLT;
  uint16_t voltage_value = (batt_voltage * DAC_MAX_VAL) / 3137 / PIX_VOLTAGE_DIVIDER; // in theory should be 3300, calibrating here to keep divider 10 in pixhawk

  HAL_DAC_SetValue(pix_pwr_hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, current_value);
  HAL_DAC_SetValue(pix_pwr_hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, voltage_value);

}

void add_msg_param_value(uint8_t* buffer, uint16_t* offset, uint16_t paramIndex) {
	mavlink_message_t msg;
	mavlink_msg_param_value_pack(1, 158, &msg, parameters[paramIndex].id, parameters[paramIndex].value.param_float, parameters[paramIndex].type, PARAM_COUNT, paramIndex);
	*offset += mavlink_msg_to_send_buffer(buffer + *offset, &msg);
}
