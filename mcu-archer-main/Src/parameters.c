#include "parameters.h"
#include <string.h>
#include <assert.h>

// Not all types are implemented, to keep code size down
#define PARAM(ID, VAR, DEF) { strcpy( parameters[index].id, ID); parameters[index].type = MAV_PARAM_TYPE_##VAR; \
	switch(parameters[index].type) { \
	case MAV_PARAM_TYPE_UINT8: parameters[index].value.param_uint8 = DEF; break; \
	case MAV_PARAM_TYPE_INT8: parameters[index].value.param_int8 = DEF; break; \
	case MAV_PARAM_TYPE_UINT16: parameters[index].value.param_uint16 = DEF; break; \
	case MAV_PARAM_TYPE_INT16: parameters[index].value.param_int16 = DEF; break; \
	case MAV_PARAM_TYPE_REAL32: parameters[index].value.param_float = DEF; break; \
	} index++;}

void paramInit() {
	uint16_t index = 0; // Only modify with the PARAM macro

	// @Param: WUT_TEMP1_LOC
	// @DisplayName: Temperature 1 location
	// @Description: Meaning of temperature measured by thermometer 1. This sets values sent in ARCHER_TEMPERATURE message
	// @User: Standard
	// @Values: 0:Unknown, 1:Ambient air, 10:Motor main, 11:Motor tail 1, 12:Motor tail 2
	PARAM("WUT_TEMP1_LOC", UINT8, 0)

	// @Param: WUT_TEMP2_LOC
	// @DisplayName: Temperature 2 location
	// @Description: Meaning of temperature measured by thermometer 2. This sets values sent in ARCHER_TEMPERATURE message
	// @User: Standard
	// @Values: 0:Unknown, 1:Ambient air, 10:Motor main, 11:Motor tail 1, 12:Motor tail 2
	PARAM("WUT_TEMP2_LOC", UINT8, 1)

	// @Param: WUT_OW_READ_ROM
	// @DisplayName: 1Wire Read ROM mode
	// @Description: Switches 1Wire bus to only send Read ROM command after reset pulse. Useful for reading ROM of new devices with a logic analyzer
	// @User: Advanced
	// @Values: 0:Disabled, 1:Enabled
	PARAM("WUT_OW_READ_ROM", UINT8, 0)

	assert(index == PARAM_COUNT && "PARAM_COUNT should equal amount of parameters defined with PARAM macro");
}

void findParam(char* id, int16_t* index, param_t** param) {
	*index = -1;
	for(uint16_t i=0; i<PARAM_COUNT; i++) {
		if(strcmp(id, parameters[i].id) == 0) {
			*index = i;
			*param = &parameters[i];
		}
	}
}

param_t* paramGetById(char* id) {
	int16_t index = 0;
	param_t* param = 0; // null pointer
	findParam(id, &index, &param);
	return param;
}

int16_t paramIndexGetById(char* id) {
	int16_t index = 0;
	param_t* param = 0; // null pointer
	findParam(id, &index, &param);
	return index;
}
