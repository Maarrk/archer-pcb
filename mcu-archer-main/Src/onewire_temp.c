/*
 * onewire_temp.c
 *
 *  Created on: Aug 6, 2020
 *      Author: Marek
 */

#include "onewire_temp.h"
#include "onewire_devices.h"

#define CMD_READ_ROM 0x33
#define CMD_MATCH_ROM 0x55
#define CMD_SKIP_ROM 0xCC

#define CMD_CONVERT_T 0x44
#define CMD_READ_SCRATCHPAD 0xBE

const uint8_t OW_DEVICE_COUNT = OW_ROM_COUNT;

uint8_t OW_ROM_table[8] = {0};
uint8_t OW_scratchpad_table[9] = {0};

uint8_t dowcrc;
uint8_t dscrc_table[] = {
		0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
		157,195, 33,127,252,162, 64, 30, 95, 1,227,189, 62, 96,130,220,
		35,125,159,193, 66, 28,254,160,225,191, 93, 3,128,222, 60, 98,
		190,224, 2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
		70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89, 7,
		219,133,103, 57,186,228, 6, 88, 25, 71,165,251,120, 38,196,154,
		101, 59,217,135, 4, 90,184,230,167,249, 27, 69,198,152,122, 36,
		248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91, 5,231,185,
		140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
		17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
		175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
		50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
		202,148,118, 40,171,245, 23, 73, 8, 86,180,234,105, 55,213,139,
		87, 9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
		233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
		116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53};
uint8_t ow_crc(uint8_t x)
{
	dowcrc = dscrc_table[dowcrc^x];
	return dowcrc;
}

void write_bit(uint8_t bit);
uint8_t read_bit();
void write_byte(uint8_t byte);
uint8_t read_byte();

void delay_us(uint16_t microseconds) {
	volatile uint16_t target_value = us_looping_htim->Instance->CNT + microseconds; // volatile should prevent optimization
	while(us_looping_htim->Instance->CNT < target_value) {
		//do nothing
	}
}

uint8_t OW_InitSequence() {
	HAL_GPIO_WritePin(OW_DQ_PORT, OW_DQ_PIN, GPIO_PIN_RESET);
	delay_us(500);
	HAL_GPIO_WritePin(OW_DQ_PORT, OW_DQ_PIN, GPIO_PIN_SET);
	delay_us(69);
	uint8_t pinStatus = HAL_GPIO_ReadPin(OW_DQ_PORT, OW_DQ_PIN);
	delay_us(430); // Wait until end of Master Rx time
	if(pinStatus == 0) return 1;
	else return 0;
}

uint8_t OW_ReadROM() {
	write_byte(CMD_READ_ROM);
	for (uint8_t i=0; i<8; i++) {
		OW_ROM_table[i] = read_byte();
	}

	dowcrc = 0; // reset CRC
	for (uint8_t i=0; i<7; i++) {
		ow_crc(OW_ROM_table[i]);
	}
	if(dowcrc == OW_ROM_table[7]) {
		return 1;
	} else {
		return 0;
	}
}

void OW_SelectAll() {
	write_byte(CMD_SKIP_ROM);
}

void OW_SelectDevice(uint8_t deviceIndex) {
	write_byte(CMD_MATCH_ROM);
	for (uint8_t i=0; i<8; i++) {
		write_byte(rom_numbers[deviceIndex][rom_order[i]]);
	}
}

void OW_ConvertTemperature() {
	write_byte(CMD_CONVERT_T);
}

uint8_t OW_ReadScratchpad() {
	write_byte(CMD_READ_SCRATCHPAD);
	for (uint8_t i=0; i<9; i++) {
		OW_scratchpad_table[i] = read_byte();
	}

	dowcrc = 0; // reset CRC
	for (uint8_t i=0; i<8; i++) {
		ow_crc(OW_scratchpad_table[i]);
	}
	if(dowcrc == OW_scratchpad_table[8]) {
		return 1;
	} else {
		return 0;
	}
}

int16_t OW_ReadTemperature() {
	write_byte(CMD_READ_SCRATCHPAD);
	uint16_t sc_lsb, sc_msb; // will be shifted around later, prevent casting
	sc_lsb = read_byte(); // scratchpad least significant byte
	sc_msb = read_byte();

	if(sc_lsb == 0x00FF && sc_msb == 0x00FF) return 0xFFFF; // No device responded

	int16_t temp = 0;
	if(sc_msb & 0x00F8) temp -= 0x0200; // Sign bits are SSSS S000 in MSB
	temp += (sc_msb & 0x0007) << 6;
	temp += (sc_lsb) >> 2;
	temp = temp * 25;
	if(sc_lsb & 0x0002) temp += 13;
	if(sc_lsb & 0x0001) temp += 6;
	return temp;
}

void write_bit(uint8_t bit) {
	HAL_GPIO_WritePin(OW_DQ_PORT, OW_DQ_PIN, GPIO_PIN_RESET); // pull DQ low to start timeslot
	if(bit == 1) HAL_GPIO_WritePin(OW_DQ_PORT, OW_DQ_PIN, GPIO_PIN_SET); // return DQ high if write 1
	delay_us(69); // hold value for remainder of timeslot
	HAL_GPIO_WritePin(OW_DQ_PORT, OW_DQ_PIN, GPIO_PIN_SET);
}

void write_byte(uint8_t byte)
{
	uint8_t temp;
	for (uint8_t i=0; i<8; i++) { // writes byte, one bit at a time
		temp = byte>>i; // shifts val right 'i' spaces
		temp &= 0x01; // copy that bit to temp
		write_bit(temp); // write bit in temp into
	}
	delay_us(69);
}

uint8_t read_bit() {
	HAL_GPIO_WritePin(OW_DQ_PORT, OW_DQ_PIN, GPIO_PIN_RESET); // pull DQ low to start timeslot
	HAL_GPIO_WritePin(OW_DQ_PORT, OW_DQ_PIN, GPIO_PIN_SET); // then return high
	delay_us(5); // delay 15us from start of timeslot
	return HAL_GPIO_ReadPin(OW_DQ_PORT, OW_DQ_PIN); // return value of DQ line
}

uint8_t read_byte() {
	uint8_t value = 0;
	for (uint8_t i=0; i<8; i++) {
		if(read_bit()) value|=0x01<<i; // reads byte in, one byte at a time and then shifts it left
		delay_us(69); // wait for rest of timeslot
	}
	return(value);
}
