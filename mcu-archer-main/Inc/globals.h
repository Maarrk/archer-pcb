#ifndef GLOBALS_H
#define GLOBALS_H

#include "main.h"
#include "../../mavlink/include/archer/mavlink.h"

uint16_t time_boot_high_bits;
TIM_HandleTypeDef* time_boot_htim;

UART_HandleTypeDef* companion_computer_huart;

DAC_HandleTypeDef* pix_pwr_hdac;

uint8_t received_char;
mavlink_message_t received_msg_powerboard;
mavlink_message_t received_msg_companion;
mavlink_status_t received_status;

mavlink_message_t sent_msg;
uint8_t sent_msg_buf[256]; // messages in main * max message length = 4 * 64
uint16_t sent_msg_len;

// HEARTBEAT
#define STATE_TIMEOUT 5000 // time in milliseconds before a measurement is considered old
uint32_t state_last_powerboard_time;
uint32_t state_last_batt_time;
uint32_t state_last_rpm_time;
uint32_t state_last_temp_time;
uint32_t state_last_angle_time;
uint32_t state_last_aero_time;
uint32_t state_last_syncpwm_time;
uint16_t syncpwm_rising_time;
uint16_t syncpwm_falling_time;
uint16_t syncpwm_value;
uint16_t syncpwm_tmp;

//BATTERY
mavlink_archer_battery_t batt_msg;
uint16_t batt_voltage;
uint16_t batt_current_main;
uint16_t batt_current_tail1;
uint16_t batt_current_tail2;
uint16_t batt_current_servo;
uint16_t batt_current_avio;

#define PIX_VOLTAGE_DIVIDER 10
#define PIX_AMPERES_PER_VOLT 30
#define DAC_MAX_VAL 4096

//RPM
uint32_t main_pulse_count;
uint16_t main_hertz;
uint32_t tail1_pulse_count;
uint16_t tail1_hertz;
uint32_t tail2_pulse_count;
uint16_t tail2_hertz;
#define PERIOD 1000 // period between calculations of motors in milliseconds
#define PERIOD_SCALE 1000 // time in seconds = PERIOD / PERIOD_SCALE
#define MAIN_POLE_COUNT 4
#define TAIL1_POLE_COUNT 6
#define TAIL2_POLE_COUNT 6


#endif //GLOBALS_H
