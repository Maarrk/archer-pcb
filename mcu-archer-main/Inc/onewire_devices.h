#ifndef ONEWIRE_DEVICES_H
#define ONEWIRE_DEVICES_H

#define OW_ROM_COUNT 2

unsigned char rom_numbers[2][8] = {
		{0x28, 0x99, 0x62, 0x64, 0x00, 0x00, 0x0B, 0x20}, // plug J01
		{0x28, 0x9A, 0x2E, 0x11, 0x00, 0x00, 0x0B, 0x13} // plug J06
};
unsigned char rom_order[8] = {0, 3, 2, 1, 6, 5, 4, 7};

#endif //ONEWIRE_DEVICES_H
