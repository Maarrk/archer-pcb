#ifndef UTILITY_H
#define UTILITY_H

#include <stdint.h>
#include <stdbool.h>

#include "../../mavlink/include/archer/mavlink.h"

void calculate_motor_speed();

uint32_t get_time_boot();

bool is_recent(uint32_t state_last_time);

void handle_mavlink_char(uint8_t received_char, mavlink_message_t* received_msg);

mavlink_message_t create_msg_heartbeat();

void handle_dac_pix();

void add_msg_param_value(uint8_t* buffer, uint16_t* offset, uint16_t paramIndex);

#endif //UTILITY_H
