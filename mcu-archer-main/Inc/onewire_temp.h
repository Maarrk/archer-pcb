#ifndef ONEWIRE_TEMP_H
#define ONEWIRE_TEMP_H

#include "stm32f1xx_hal.h"

TIM_HandleTypeDef* us_looping_htim;
GPIO_TypeDef* OW_DQ_PORT;
uint16_t OW_DQ_PIN;
uint8_t OW_ROM_table[8];
const uint8_t OW_DEVICE_COUNT;

void delay_us(uint16_t microseconds);

uint8_t OW_InitSequence();

uint8_t OW_ReadROM();

void OW_SelectAll();
void OW_SelectDevice(uint8_t deviceIndex);

void OW_ConvertTemperature();
uint8_t OW_ReadScratchpad();
int16_t OW_ReadTemperature();

#endif //ONEWIRE_TEMP_H
