# Pixhawk notes

Information on Pixhawk setup for use with ARCHER electronics

## TODO:

- List PWM channel usage

## Battery monitor

Use POWER1 or POWER2 connector on Pixhawk cube.

### Mission Planner

- Set type to `Other`
- Set voltage divider to `10`
- Set amperes per volt to `30`
