# Mavlink notes

Plans and information on using mavlink to communicate between on-board electronics

## Conventions:

- Use Mavlink 2
- Trailing zeros truncated
    - But actually after reordering
- Implement messages starting at 44k

## Dialect design:

- 44000 ARCHER_HEARTBEAT - see if relevant, maybe [HEARTBEAT (#0)](https://mavlink.io/en/messages/minimal.html#HEARTBEAT) is OK
- 44001 ARCHER_FULL - all collected data
    - add some degree of futureproofing
    - maybe set non-measured values to a known constant to prevent truncation
- 44002 ARCHER_BATTERY - battery voltage and list of currents
    - Follow [BATTERY_STATUS (#147)](https://mavlink.io/en/messages/common.html#BATTERY_STATUS) where applicable
- 44003 ARCHER_RPM
    - Note that [ardupilotmega RPM (#226)](https://mavlink.io/en/messages/ardupilotmega.html#RPM) already supports two sensors
- 44004 ARCHER_TEMPERATURE - measured temperature
    - Separate message for each temperature, since there might be multiple thermometers of different types of (akin to BATTERY_STATUS design)
- 44005 ARCHER_SERVO - measured positions of servomotors
    - Maybe rework to ARCHER_SWASHPLATE
- 44006 ARCHER_AERODYNAMIC - airspeed and angles