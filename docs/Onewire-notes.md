# Onewire notes

Information on used 1Wire equipment

## DS18B20 thermometers

### Connecting

Connect the thermometer plug as a male servo connector - the contact should be visible on opposite sides of each plug. Connect female-female servo cable to mainboard with ground connected to ground - servo power lead acts as 1Wire DQ, servo signal lead acts as 3V3 power.

### Device list

- Number (our ID) - ROM (`Family SerNumLSB SerNumMSB CRC`) - Plug identification
- 1 - 28 996264 00000B 20 - J01
- 2 - 28 9A2E11 00000B 13 - J06
