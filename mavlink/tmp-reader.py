#!/usr/bin/env python3
import time
import serial
from pymavlink.dialects.v20 import archer

# fin = open('/tmp/archer-serial-reader-output', 'rb')
mav = archer.MAVLink(open('/tmp/archer-tmp-reader-output', 'wb'))

previous_seq = 0
increment = 0

last_time_boot = 0
last_sync_pwm = 0

filename = 'logs/archer-serial-reader-output-20200629T132943' 

with open(filename + '.bin', 'rb') as fin:
    with open(filename + '.csv', 'w') as fout:

        fout.write('seq;voltage[mV];current_motor_main[cA];current_motor_tail1[cA];current_motor_tail2[cA];current_servo[cA];current_avio[cA];time_boot[ms];sync_pwm[us]\n')

        for byte in iter(lambda: fin.read(1), b''):
            try:

                msgs = mav.parse_buffer(byte)
                if msgs is not None and len(msgs) > 0:
                    for  msg in msgs:
                        # Detect looping seq number and change to continuous
                        if msg.get_seq() < previous_seq:
                            increment += 256
                        previous_seq = msg.get_seq()

                        if msg.get_msgId() == archer.MAVLINK_MSG_ID_ARCHER_HEARTBEAT:
                            last_time_boot = msg.time_boot_ms
                            last_sync_pwm = msg.sync_pwm
                        elif msg.get_msgId() == archer.MAVLINK_MSG_ID_ARCHER_BATTERY:
                            text = '{};{};{};{};{};{};{};{};{}\n'.format(msg.get_seq() + increment, msg.voltage,
                            msg.current_motor_main, msg.current_motor_tail1, msg.current_motor_tail2,
                            msg.current_servo, msg.current_avio,
                            last_time_boot, last_sync_pwm)

                            print(text)
                            fout.write(text)
                        # mav.send(msg)
            except Exception as e:
                print('MAVLink error: {}'.format(e))
