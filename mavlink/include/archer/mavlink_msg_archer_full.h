#pragma once
// MESSAGE ARCHER_FULL PACKING

#define MAVLINK_MSG_ID_ARCHER_FULL 44001

MAVPACKED(
typedef struct __mavlink_archer_full_t {
 uint16_t voltage; /*< [mV] Battery voltage.*/
 uint16_t current; /*< [cA] Current consumed from battery.*/
 uint16_t rpm_main; /*< [rpm] Main motor revolutions per minute*/
}) mavlink_archer_full_t;

#define MAVLINK_MSG_ID_ARCHER_FULL_LEN 6
#define MAVLINK_MSG_ID_ARCHER_FULL_MIN_LEN 6
#define MAVLINK_MSG_ID_44001_LEN 6
#define MAVLINK_MSG_ID_44001_MIN_LEN 6

#define MAVLINK_MSG_ID_ARCHER_FULL_CRC 112
#define MAVLINK_MSG_ID_44001_CRC 112



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ARCHER_FULL { \
    44001, \
    "ARCHER_FULL", \
    3, \
    {  { "voltage", NULL, MAVLINK_TYPE_UINT16_T, 0, 0, offsetof(mavlink_archer_full_t, voltage) }, \
         { "current", NULL, MAVLINK_TYPE_UINT16_T, 0, 2, offsetof(mavlink_archer_full_t, current) }, \
         { "rpm_main", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_archer_full_t, rpm_main) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ARCHER_FULL { \
    "ARCHER_FULL", \
    3, \
    {  { "voltage", NULL, MAVLINK_TYPE_UINT16_T, 0, 0, offsetof(mavlink_archer_full_t, voltage) }, \
         { "current", NULL, MAVLINK_TYPE_UINT16_T, 0, 2, offsetof(mavlink_archer_full_t, current) }, \
         { "rpm_main", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_archer_full_t, rpm_main) }, \
         } \
}
#endif

/**
 * @brief Pack a archer_full message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param voltage [mV] Battery voltage.
 * @param current [cA] Current consumed from battery.
 * @param rpm_main [rpm] Main motor revolutions per minute
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_full_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint16_t voltage, uint16_t current, uint16_t rpm_main)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_FULL_LEN];
    _mav_put_uint16_t(buf, 0, voltage);
    _mav_put_uint16_t(buf, 2, current);
    _mav_put_uint16_t(buf, 4, rpm_main);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_FULL_LEN);
#else
    mavlink_archer_full_t packet;
    packet.voltage = voltage;
    packet.current = current;
    packet.rpm_main = rpm_main;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_FULL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_FULL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ARCHER_FULL_MIN_LEN, MAVLINK_MSG_ID_ARCHER_FULL_LEN, MAVLINK_MSG_ID_ARCHER_FULL_CRC);
}

/**
 * @brief Pack a archer_full message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param voltage [mV] Battery voltage.
 * @param current [cA] Current consumed from battery.
 * @param rpm_main [rpm] Main motor revolutions per minute
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_full_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint16_t voltage,uint16_t current,uint16_t rpm_main)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_FULL_LEN];
    _mav_put_uint16_t(buf, 0, voltage);
    _mav_put_uint16_t(buf, 2, current);
    _mav_put_uint16_t(buf, 4, rpm_main);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_FULL_LEN);
#else
    mavlink_archer_full_t packet;
    packet.voltage = voltage;
    packet.current = current;
    packet.rpm_main = rpm_main;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_FULL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_FULL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ARCHER_FULL_MIN_LEN, MAVLINK_MSG_ID_ARCHER_FULL_LEN, MAVLINK_MSG_ID_ARCHER_FULL_CRC);
}

/**
 * @brief Encode a archer_full struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param archer_full C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_full_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_archer_full_t* archer_full)
{
    return mavlink_msg_archer_full_pack(system_id, component_id, msg, archer_full->voltage, archer_full->current, archer_full->rpm_main);
}

/**
 * @brief Encode a archer_full struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param archer_full C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_full_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_archer_full_t* archer_full)
{
    return mavlink_msg_archer_full_pack_chan(system_id, component_id, chan, msg, archer_full->voltage, archer_full->current, archer_full->rpm_main);
}

/**
 * @brief Send a archer_full message
 * @param chan MAVLink channel to send the message
 *
 * @param voltage [mV] Battery voltage.
 * @param current [cA] Current consumed from battery.
 * @param rpm_main [rpm] Main motor revolutions per minute
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_archer_full_send(mavlink_channel_t chan, uint16_t voltage, uint16_t current, uint16_t rpm_main)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_FULL_LEN];
    _mav_put_uint16_t(buf, 0, voltage);
    _mav_put_uint16_t(buf, 2, current);
    _mav_put_uint16_t(buf, 4, rpm_main);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_FULL, buf, MAVLINK_MSG_ID_ARCHER_FULL_MIN_LEN, MAVLINK_MSG_ID_ARCHER_FULL_LEN, MAVLINK_MSG_ID_ARCHER_FULL_CRC);
#else
    mavlink_archer_full_t packet;
    packet.voltage = voltage;
    packet.current = current;
    packet.rpm_main = rpm_main;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_FULL, (const char *)&packet, MAVLINK_MSG_ID_ARCHER_FULL_MIN_LEN, MAVLINK_MSG_ID_ARCHER_FULL_LEN, MAVLINK_MSG_ID_ARCHER_FULL_CRC);
#endif
}

/**
 * @brief Send a archer_full message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_archer_full_send_struct(mavlink_channel_t chan, const mavlink_archer_full_t* archer_full)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_archer_full_send(chan, archer_full->voltage, archer_full->current, archer_full->rpm_main);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_FULL, (const char *)archer_full, MAVLINK_MSG_ID_ARCHER_FULL_MIN_LEN, MAVLINK_MSG_ID_ARCHER_FULL_LEN, MAVLINK_MSG_ID_ARCHER_FULL_CRC);
#endif
}

#if MAVLINK_MSG_ID_ARCHER_FULL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_archer_full_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint16_t voltage, uint16_t current, uint16_t rpm_main)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint16_t(buf, 0, voltage);
    _mav_put_uint16_t(buf, 2, current);
    _mav_put_uint16_t(buf, 4, rpm_main);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_FULL, buf, MAVLINK_MSG_ID_ARCHER_FULL_MIN_LEN, MAVLINK_MSG_ID_ARCHER_FULL_LEN, MAVLINK_MSG_ID_ARCHER_FULL_CRC);
#else
    mavlink_archer_full_t *packet = (mavlink_archer_full_t *)msgbuf;
    packet->voltage = voltage;
    packet->current = current;
    packet->rpm_main = rpm_main;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_FULL, (const char *)packet, MAVLINK_MSG_ID_ARCHER_FULL_MIN_LEN, MAVLINK_MSG_ID_ARCHER_FULL_LEN, MAVLINK_MSG_ID_ARCHER_FULL_CRC);
#endif
}
#endif

#endif

// MESSAGE ARCHER_FULL UNPACKING


/**
 * @brief Get field voltage from archer_full message
 *
 * @return [mV] Battery voltage.
 */
static inline uint16_t mavlink_msg_archer_full_get_voltage(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  0);
}

/**
 * @brief Get field current from archer_full message
 *
 * @return [cA] Current consumed from battery.
 */
static inline uint16_t mavlink_msg_archer_full_get_current(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  2);
}

/**
 * @brief Get field rpm_main from archer_full message
 *
 * @return [rpm] Main motor revolutions per minute
 */
static inline uint16_t mavlink_msg_archer_full_get_rpm_main(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  4);
}

/**
 * @brief Decode a archer_full message into a struct
 *
 * @param msg The message to decode
 * @param archer_full C-struct to decode the message contents into
 */
static inline void mavlink_msg_archer_full_decode(const mavlink_message_t* msg, mavlink_archer_full_t* archer_full)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    archer_full->voltage = mavlink_msg_archer_full_get_voltage(msg);
    archer_full->current = mavlink_msg_archer_full_get_current(msg);
    archer_full->rpm_main = mavlink_msg_archer_full_get_rpm_main(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ARCHER_FULL_LEN? msg->len : MAVLINK_MSG_ID_ARCHER_FULL_LEN;
        memset(archer_full, 0, MAVLINK_MSG_ID_ARCHER_FULL_LEN);
    memcpy(archer_full, _MAV_PAYLOAD(msg), len);
#endif
}
