#pragma once
// MESSAGE ARCHER_HEARTBEAT PACKING

#define MAVLINK_MSG_ID_ARCHER_HEARTBEAT 44000

MAVPACKED(
typedef struct __mavlink_archer_heartbeat_t {
 uint32_t time_boot_ms; /*< [ms] Timestamp (time since system boot).*/
 uint16_t status; /*<  See the ARCHER_STATE enum.*/
 uint16_t sync_pwm; /*< [us] Synchronising PWM from autopilot value.*/
}) mavlink_archer_heartbeat_t;

#define MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN 8
#define MAVLINK_MSG_ID_ARCHER_HEARTBEAT_MIN_LEN 8
#define MAVLINK_MSG_ID_44000_LEN 8
#define MAVLINK_MSG_ID_44000_MIN_LEN 8

#define MAVLINK_MSG_ID_ARCHER_HEARTBEAT_CRC 6
#define MAVLINK_MSG_ID_44000_CRC 6



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ARCHER_HEARTBEAT { \
    44000, \
    "ARCHER_HEARTBEAT", \
    3, \
    {  { "time_boot_ms", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_archer_heartbeat_t, time_boot_ms) }, \
         { "status", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_archer_heartbeat_t, status) }, \
         { "sync_pwm", NULL, MAVLINK_TYPE_UINT16_T, 0, 6, offsetof(mavlink_archer_heartbeat_t, sync_pwm) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ARCHER_HEARTBEAT { \
    "ARCHER_HEARTBEAT", \
    3, \
    {  { "time_boot_ms", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_archer_heartbeat_t, time_boot_ms) }, \
         { "status", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_archer_heartbeat_t, status) }, \
         { "sync_pwm", NULL, MAVLINK_TYPE_UINT16_T, 0, 6, offsetof(mavlink_archer_heartbeat_t, sync_pwm) }, \
         } \
}
#endif

/**
 * @brief Pack a archer_heartbeat message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time_boot_ms [ms] Timestamp (time since system boot).
 * @param status  See the ARCHER_STATE enum.
 * @param sync_pwm [us] Synchronising PWM from autopilot value.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_heartbeat_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time_boot_ms, uint16_t status, uint16_t sync_pwm)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN];
    _mav_put_uint32_t(buf, 0, time_boot_ms);
    _mav_put_uint16_t(buf, 4, status);
    _mav_put_uint16_t(buf, 6, sync_pwm);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN);
#else
    mavlink_archer_heartbeat_t packet;
    packet.time_boot_ms = time_boot_ms;
    packet.status = status;
    packet.sync_pwm = sync_pwm;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_HEARTBEAT;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_MIN_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_CRC);
}

/**
 * @brief Pack a archer_heartbeat message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time_boot_ms [ms] Timestamp (time since system boot).
 * @param status  See the ARCHER_STATE enum.
 * @param sync_pwm [us] Synchronising PWM from autopilot value.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_heartbeat_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time_boot_ms,uint16_t status,uint16_t sync_pwm)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN];
    _mav_put_uint32_t(buf, 0, time_boot_ms);
    _mav_put_uint16_t(buf, 4, status);
    _mav_put_uint16_t(buf, 6, sync_pwm);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN);
#else
    mavlink_archer_heartbeat_t packet;
    packet.time_boot_ms = time_boot_ms;
    packet.status = status;
    packet.sync_pwm = sync_pwm;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_HEARTBEAT;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_MIN_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_CRC);
}

/**
 * @brief Encode a archer_heartbeat struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param archer_heartbeat C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_heartbeat_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_archer_heartbeat_t* archer_heartbeat)
{
    return mavlink_msg_archer_heartbeat_pack(system_id, component_id, msg, archer_heartbeat->time_boot_ms, archer_heartbeat->status, archer_heartbeat->sync_pwm);
}

/**
 * @brief Encode a archer_heartbeat struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param archer_heartbeat C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_heartbeat_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_archer_heartbeat_t* archer_heartbeat)
{
    return mavlink_msg_archer_heartbeat_pack_chan(system_id, component_id, chan, msg, archer_heartbeat->time_boot_ms, archer_heartbeat->status, archer_heartbeat->sync_pwm);
}

/**
 * @brief Send a archer_heartbeat message
 * @param chan MAVLink channel to send the message
 *
 * @param time_boot_ms [ms] Timestamp (time since system boot).
 * @param status  See the ARCHER_STATE enum.
 * @param sync_pwm [us] Synchronising PWM from autopilot value.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_archer_heartbeat_send(mavlink_channel_t chan, uint32_t time_boot_ms, uint16_t status, uint16_t sync_pwm)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN];
    _mav_put_uint32_t(buf, 0, time_boot_ms);
    _mav_put_uint16_t(buf, 4, status);
    _mav_put_uint16_t(buf, 6, sync_pwm);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_HEARTBEAT, buf, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_MIN_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_CRC);
#else
    mavlink_archer_heartbeat_t packet;
    packet.time_boot_ms = time_boot_ms;
    packet.status = status;
    packet.sync_pwm = sync_pwm;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_HEARTBEAT, (const char *)&packet, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_MIN_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_CRC);
#endif
}

/**
 * @brief Send a archer_heartbeat message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_archer_heartbeat_send_struct(mavlink_channel_t chan, const mavlink_archer_heartbeat_t* archer_heartbeat)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_archer_heartbeat_send(chan, archer_heartbeat->time_boot_ms, archer_heartbeat->status, archer_heartbeat->sync_pwm);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_HEARTBEAT, (const char *)archer_heartbeat, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_MIN_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_CRC);
#endif
}

#if MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_archer_heartbeat_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time_boot_ms, uint16_t status, uint16_t sync_pwm)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time_boot_ms);
    _mav_put_uint16_t(buf, 4, status);
    _mav_put_uint16_t(buf, 6, sync_pwm);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_HEARTBEAT, buf, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_MIN_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_CRC);
#else
    mavlink_archer_heartbeat_t *packet = (mavlink_archer_heartbeat_t *)msgbuf;
    packet->time_boot_ms = time_boot_ms;
    packet->status = status;
    packet->sync_pwm = sync_pwm;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_HEARTBEAT, (const char *)packet, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_MIN_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_CRC);
#endif
}
#endif

#endif

// MESSAGE ARCHER_HEARTBEAT UNPACKING


/**
 * @brief Get field time_boot_ms from archer_heartbeat message
 *
 * @return [ms] Timestamp (time since system boot).
 */
static inline uint32_t mavlink_msg_archer_heartbeat_get_time_boot_ms(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field status from archer_heartbeat message
 *
 * @return  See the ARCHER_STATE enum.
 */
static inline uint16_t mavlink_msg_archer_heartbeat_get_status(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  4);
}

/**
 * @brief Get field sync_pwm from archer_heartbeat message
 *
 * @return [us] Synchronising PWM from autopilot value.
 */
static inline uint16_t mavlink_msg_archer_heartbeat_get_sync_pwm(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  6);
}

/**
 * @brief Decode a archer_heartbeat message into a struct
 *
 * @param msg The message to decode
 * @param archer_heartbeat C-struct to decode the message contents into
 */
static inline void mavlink_msg_archer_heartbeat_decode(const mavlink_message_t* msg, mavlink_archer_heartbeat_t* archer_heartbeat)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    archer_heartbeat->time_boot_ms = mavlink_msg_archer_heartbeat_get_time_boot_ms(msg);
    archer_heartbeat->status = mavlink_msg_archer_heartbeat_get_status(msg);
    archer_heartbeat->sync_pwm = mavlink_msg_archer_heartbeat_get_sync_pwm(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN? msg->len : MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN;
        memset(archer_heartbeat, 0, MAVLINK_MSG_ID_ARCHER_HEARTBEAT_LEN);
    memcpy(archer_heartbeat, _MAV_PAYLOAD(msg), len);
#endif
}
