#pragma once
// MESSAGE ARCHER_BATTERY PACKING

#define MAVLINK_MSG_ID_ARCHER_BATTERY 44002

MAVPACKED(
typedef struct __mavlink_archer_battery_t {
 uint16_t voltage; /*< [mV] Battery voltage.*/
 uint16_t current_motor_main; /*< [cA] Current consumed by main motor.*/
 uint16_t current_motor_tail1; /*< [cA] Current consumed by 1st tail motor.*/
 uint16_t current_motor_tail2; /*< [cA] Current consumed by 2nd tail motor.*/
 uint16_t current_servo; /*< [cA] Current consumed by servomechanism power rail.*/
 uint16_t current_avio; /*< [cA] Current consumed by avionics power rail.*/
}) mavlink_archer_battery_t;

#define MAVLINK_MSG_ID_ARCHER_BATTERY_LEN 12
#define MAVLINK_MSG_ID_ARCHER_BATTERY_MIN_LEN 12
#define MAVLINK_MSG_ID_44002_LEN 12
#define MAVLINK_MSG_ID_44002_MIN_LEN 12

#define MAVLINK_MSG_ID_ARCHER_BATTERY_CRC 201
#define MAVLINK_MSG_ID_44002_CRC 201



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ARCHER_BATTERY { \
    44002, \
    "ARCHER_BATTERY", \
    6, \
    {  { "voltage", NULL, MAVLINK_TYPE_UINT16_T, 0, 0, offsetof(mavlink_archer_battery_t, voltage) }, \
         { "current_motor_main", NULL, MAVLINK_TYPE_UINT16_T, 0, 2, offsetof(mavlink_archer_battery_t, current_motor_main) }, \
         { "current_motor_tail1", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_archer_battery_t, current_motor_tail1) }, \
         { "current_motor_tail2", NULL, MAVLINK_TYPE_UINT16_T, 0, 6, offsetof(mavlink_archer_battery_t, current_motor_tail2) }, \
         { "current_servo", NULL, MAVLINK_TYPE_UINT16_T, 0, 8, offsetof(mavlink_archer_battery_t, current_servo) }, \
         { "current_avio", NULL, MAVLINK_TYPE_UINT16_T, 0, 10, offsetof(mavlink_archer_battery_t, current_avio) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ARCHER_BATTERY { \
    "ARCHER_BATTERY", \
    6, \
    {  { "voltage", NULL, MAVLINK_TYPE_UINT16_T, 0, 0, offsetof(mavlink_archer_battery_t, voltage) }, \
         { "current_motor_main", NULL, MAVLINK_TYPE_UINT16_T, 0, 2, offsetof(mavlink_archer_battery_t, current_motor_main) }, \
         { "current_motor_tail1", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_archer_battery_t, current_motor_tail1) }, \
         { "current_motor_tail2", NULL, MAVLINK_TYPE_UINT16_T, 0, 6, offsetof(mavlink_archer_battery_t, current_motor_tail2) }, \
         { "current_servo", NULL, MAVLINK_TYPE_UINT16_T, 0, 8, offsetof(mavlink_archer_battery_t, current_servo) }, \
         { "current_avio", NULL, MAVLINK_TYPE_UINT16_T, 0, 10, offsetof(mavlink_archer_battery_t, current_avio) }, \
         } \
}
#endif

/**
 * @brief Pack a archer_battery message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param voltage [mV] Battery voltage.
 * @param current_motor_main [cA] Current consumed by main motor.
 * @param current_motor_tail1 [cA] Current consumed by 1st tail motor.
 * @param current_motor_tail2 [cA] Current consumed by 2nd tail motor.
 * @param current_servo [cA] Current consumed by servomechanism power rail.
 * @param current_avio [cA] Current consumed by avionics power rail.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_battery_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint16_t voltage, uint16_t current_motor_main, uint16_t current_motor_tail1, uint16_t current_motor_tail2, uint16_t current_servo, uint16_t current_avio)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_BATTERY_LEN];
    _mav_put_uint16_t(buf, 0, voltage);
    _mav_put_uint16_t(buf, 2, current_motor_main);
    _mav_put_uint16_t(buf, 4, current_motor_tail1);
    _mav_put_uint16_t(buf, 6, current_motor_tail2);
    _mav_put_uint16_t(buf, 8, current_servo);
    _mav_put_uint16_t(buf, 10, current_avio);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN);
#else
    mavlink_archer_battery_t packet;
    packet.voltage = voltage;
    packet.current_motor_main = current_motor_main;
    packet.current_motor_tail1 = current_motor_tail1;
    packet.current_motor_tail2 = current_motor_tail2;
    packet.current_servo = current_servo;
    packet.current_avio = current_avio;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_BATTERY;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ARCHER_BATTERY_MIN_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_CRC);
}

/**
 * @brief Pack a archer_battery message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param voltage [mV] Battery voltage.
 * @param current_motor_main [cA] Current consumed by main motor.
 * @param current_motor_tail1 [cA] Current consumed by 1st tail motor.
 * @param current_motor_tail2 [cA] Current consumed by 2nd tail motor.
 * @param current_servo [cA] Current consumed by servomechanism power rail.
 * @param current_avio [cA] Current consumed by avionics power rail.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_battery_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint16_t voltage,uint16_t current_motor_main,uint16_t current_motor_tail1,uint16_t current_motor_tail2,uint16_t current_servo,uint16_t current_avio)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_BATTERY_LEN];
    _mav_put_uint16_t(buf, 0, voltage);
    _mav_put_uint16_t(buf, 2, current_motor_main);
    _mav_put_uint16_t(buf, 4, current_motor_tail1);
    _mav_put_uint16_t(buf, 6, current_motor_tail2);
    _mav_put_uint16_t(buf, 8, current_servo);
    _mav_put_uint16_t(buf, 10, current_avio);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN);
#else
    mavlink_archer_battery_t packet;
    packet.voltage = voltage;
    packet.current_motor_main = current_motor_main;
    packet.current_motor_tail1 = current_motor_tail1;
    packet.current_motor_tail2 = current_motor_tail2;
    packet.current_servo = current_servo;
    packet.current_avio = current_avio;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_BATTERY;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ARCHER_BATTERY_MIN_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_CRC);
}

/**
 * @brief Encode a archer_battery struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param archer_battery C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_battery_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_archer_battery_t* archer_battery)
{
    return mavlink_msg_archer_battery_pack(system_id, component_id, msg, archer_battery->voltage, archer_battery->current_motor_main, archer_battery->current_motor_tail1, archer_battery->current_motor_tail2, archer_battery->current_servo, archer_battery->current_avio);
}

/**
 * @brief Encode a archer_battery struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param archer_battery C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_battery_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_archer_battery_t* archer_battery)
{
    return mavlink_msg_archer_battery_pack_chan(system_id, component_id, chan, msg, archer_battery->voltage, archer_battery->current_motor_main, archer_battery->current_motor_tail1, archer_battery->current_motor_tail2, archer_battery->current_servo, archer_battery->current_avio);
}

/**
 * @brief Send a archer_battery message
 * @param chan MAVLink channel to send the message
 *
 * @param voltage [mV] Battery voltage.
 * @param current_motor_main [cA] Current consumed by main motor.
 * @param current_motor_tail1 [cA] Current consumed by 1st tail motor.
 * @param current_motor_tail2 [cA] Current consumed by 2nd tail motor.
 * @param current_servo [cA] Current consumed by servomechanism power rail.
 * @param current_avio [cA] Current consumed by avionics power rail.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_archer_battery_send(mavlink_channel_t chan, uint16_t voltage, uint16_t current_motor_main, uint16_t current_motor_tail1, uint16_t current_motor_tail2, uint16_t current_servo, uint16_t current_avio)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_BATTERY_LEN];
    _mav_put_uint16_t(buf, 0, voltage);
    _mav_put_uint16_t(buf, 2, current_motor_main);
    _mav_put_uint16_t(buf, 4, current_motor_tail1);
    _mav_put_uint16_t(buf, 6, current_motor_tail2);
    _mav_put_uint16_t(buf, 8, current_servo);
    _mav_put_uint16_t(buf, 10, current_avio);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_BATTERY, buf, MAVLINK_MSG_ID_ARCHER_BATTERY_MIN_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_CRC);
#else
    mavlink_archer_battery_t packet;
    packet.voltage = voltage;
    packet.current_motor_main = current_motor_main;
    packet.current_motor_tail1 = current_motor_tail1;
    packet.current_motor_tail2 = current_motor_tail2;
    packet.current_servo = current_servo;
    packet.current_avio = current_avio;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_BATTERY, (const char *)&packet, MAVLINK_MSG_ID_ARCHER_BATTERY_MIN_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_CRC);
#endif
}

/**
 * @brief Send a archer_battery message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_archer_battery_send_struct(mavlink_channel_t chan, const mavlink_archer_battery_t* archer_battery)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_archer_battery_send(chan, archer_battery->voltage, archer_battery->current_motor_main, archer_battery->current_motor_tail1, archer_battery->current_motor_tail2, archer_battery->current_servo, archer_battery->current_avio);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_BATTERY, (const char *)archer_battery, MAVLINK_MSG_ID_ARCHER_BATTERY_MIN_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_CRC);
#endif
}

#if MAVLINK_MSG_ID_ARCHER_BATTERY_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_archer_battery_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint16_t voltage, uint16_t current_motor_main, uint16_t current_motor_tail1, uint16_t current_motor_tail2, uint16_t current_servo, uint16_t current_avio)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint16_t(buf, 0, voltage);
    _mav_put_uint16_t(buf, 2, current_motor_main);
    _mav_put_uint16_t(buf, 4, current_motor_tail1);
    _mav_put_uint16_t(buf, 6, current_motor_tail2);
    _mav_put_uint16_t(buf, 8, current_servo);
    _mav_put_uint16_t(buf, 10, current_avio);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_BATTERY, buf, MAVLINK_MSG_ID_ARCHER_BATTERY_MIN_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_CRC);
#else
    mavlink_archer_battery_t *packet = (mavlink_archer_battery_t *)msgbuf;
    packet->voltage = voltage;
    packet->current_motor_main = current_motor_main;
    packet->current_motor_tail1 = current_motor_tail1;
    packet->current_motor_tail2 = current_motor_tail2;
    packet->current_servo = current_servo;
    packet->current_avio = current_avio;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_BATTERY, (const char *)packet, MAVLINK_MSG_ID_ARCHER_BATTERY_MIN_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN, MAVLINK_MSG_ID_ARCHER_BATTERY_CRC);
#endif
}
#endif

#endif

// MESSAGE ARCHER_BATTERY UNPACKING


/**
 * @brief Get field voltage from archer_battery message
 *
 * @return [mV] Battery voltage.
 */
static inline uint16_t mavlink_msg_archer_battery_get_voltage(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  0);
}

/**
 * @brief Get field current_motor_main from archer_battery message
 *
 * @return [cA] Current consumed by main motor.
 */
static inline uint16_t mavlink_msg_archer_battery_get_current_motor_main(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  2);
}

/**
 * @brief Get field current_motor_tail1 from archer_battery message
 *
 * @return [cA] Current consumed by 1st tail motor.
 */
static inline uint16_t mavlink_msg_archer_battery_get_current_motor_tail1(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  4);
}

/**
 * @brief Get field current_motor_tail2 from archer_battery message
 *
 * @return [cA] Current consumed by 2nd tail motor.
 */
static inline uint16_t mavlink_msg_archer_battery_get_current_motor_tail2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  6);
}

/**
 * @brief Get field current_servo from archer_battery message
 *
 * @return [cA] Current consumed by servomechanism power rail.
 */
static inline uint16_t mavlink_msg_archer_battery_get_current_servo(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  8);
}

/**
 * @brief Get field current_avio from archer_battery message
 *
 * @return [cA] Current consumed by avionics power rail.
 */
static inline uint16_t mavlink_msg_archer_battery_get_current_avio(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  10);
}

/**
 * @brief Decode a archer_battery message into a struct
 *
 * @param msg The message to decode
 * @param archer_battery C-struct to decode the message contents into
 */
static inline void mavlink_msg_archer_battery_decode(const mavlink_message_t* msg, mavlink_archer_battery_t* archer_battery)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    archer_battery->voltage = mavlink_msg_archer_battery_get_voltage(msg);
    archer_battery->current_motor_main = mavlink_msg_archer_battery_get_current_motor_main(msg);
    archer_battery->current_motor_tail1 = mavlink_msg_archer_battery_get_current_motor_tail1(msg);
    archer_battery->current_motor_tail2 = mavlink_msg_archer_battery_get_current_motor_tail2(msg);
    archer_battery->current_servo = mavlink_msg_archer_battery_get_current_servo(msg);
    archer_battery->current_avio = mavlink_msg_archer_battery_get_current_avio(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ARCHER_BATTERY_LEN? msg->len : MAVLINK_MSG_ID_ARCHER_BATTERY_LEN;
        memset(archer_battery, 0, MAVLINK_MSG_ID_ARCHER_BATTERY_LEN);
    memcpy(archer_battery, _MAV_PAYLOAD(msg), len);
#endif
}
