/** @file
 *  @brief MAVLink comm protocol generated from archer.xml
 *  @see http://mavlink.org
 */
#pragma once
#ifndef MAVLINK_ARCHER_H
#define MAVLINK_ARCHER_H

#ifndef MAVLINK_H
    #error Wrong include order: MAVLINK_ARCHER.H MUST NOT BE DIRECTLY USED. Include mavlink.h from the same directory instead or set ALL AND EVERY defines from MAVLINK.H manually accordingly, including the #define MAVLINK_H call.
#endif

#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#ifdef __cplusplus
extern "C" {
#endif

// MESSAGE LENGTHS AND CRCS

#ifndef MAVLINK_MESSAGE_LENGTHS
#define MAVLINK_MESSAGE_LENGTHS {}
#endif

#ifndef MAVLINK_MESSAGE_CRCS
#define MAVLINK_MESSAGE_CRCS {{0, 50, 9, 9, 0, 0, 0}, {20, 214, 20, 20, 3, 2, 3}, {21, 159, 2, 2, 3, 0, 1}, {22, 220, 25, 25, 0, 0, 0}, {23, 168, 23, 23, 3, 4, 5}, {300, 217, 22, 22, 0, 0, 0}, {44000, 6, 8, 8, 0, 0, 0}, {44001, 112, 6, 6, 0, 0, 0}, {44002, 201, 12, 12, 0, 0, 0}, {44003, 187, 6, 6, 0, 0, 0}, {44004, 107, 3, 3, 0, 0, 0}, {44005, 69, 3, 3, 0, 0, 0}}
#endif

#include "../protocol.h"

#define MAVLINK_ENABLED_ARCHER

// ENUM DEFINITIONS


/** @brief System status and taken measurements bitfield. */
#ifndef HAVE_ENUM_ARCHER_STATE
#define HAVE_ENUM_ARCHER_STATE
typedef enum ARCHER_STATE
{
   ARCHER_STATE_MAINBOARD_OK=1, /* Mainboard main program working. | */
   ARCHER_STATE_POWERBOARD_OK=2, /* Powerboard main program working. | */
   ARCHER_STATE_MEASURING_BATTERY=4, /* Battery measured recently. | */
   ARCHER_STATE_MEASURING_RPM=8, /* RPM measured recently. | */
   ARCHER_STATE_MEASURING_TEMPERATURE=16, /* Temperature measured recently. | */
   ARCHER_STATE_MEASURING_ANGLE=32, /* Angle measured recently. | */
   ARCHER_STATE_MEASURING_AERODYNAMIC=64, /* Aerodynamic conditions measured recently. | */
   ARCHER_STATE_SYNC_PWM=1024, /* Synchronising PWM from autopilot available. | */
   ARCHER_STATE_ENUM_END=1025, /*  | */
} ARCHER_STATE;
#endif

/** @brief Location of helicopter at which temperature is measured */
#ifndef HAVE_ENUM_ARCHER_TEMPERATURE_LOCATION
#define HAVE_ENUM_ARCHER_TEMPERATURE_LOCATION
typedef enum ARCHER_TEMPERATURE_LOCATION
{
   ARCHER_TEMPERATURE_LOCATION_UNKNOWN=0, /* Temperature is measured at an unknown location | */
   ARCHER_TEMPERATURE_LOCATION_AMBIENT=1, /* Temperature of ambient air | */
   ARCHER_TEMPERATURE_LOCATION_MOTOR_MAIN=10, /* Temperature of main motor | */
   ARCHER_TEMPERATURE_LOCATION_MOTOR_TAIL1=11, /* Temperature of 1st tail motor | */
   ARCHER_TEMPERATURE_LOCATION_MOTOR_TAIL2=12, /* Temperature of 2nd tail motor | */
   ARCHER_TEMPERATURE_LOCATION_ENUM_END=13, /*  | */
} ARCHER_TEMPERATURE_LOCATION;
#endif

/** @brief Identifier which angle is measured by this encoder */
#ifndef HAVE_ENUM_ARCHER_ANGLE
#define HAVE_ENUM_ARCHER_ANGLE
typedef enum ARCHER_ANGLE
{
   ARCHER_ANGLE_UNKNOWN=0, /* Unknown angle is measured | */
   ARCHER_ANGLE_SWASHPLATE1=1, /* Angle of swashplate actuator 1, positive increasing thrust. | */
   ARCHER_ANGLE_SWASHPLATE2=2, /* Angle of swashplate actuator 2, positive increasing thrust. | */
   ARCHER_ANGLE_SWASHPLATE3=3, /* Angle of swashplate actuator 3, positive increasing thrust. | */
   ARCHER_ANGLE_TAIL1=4, /* Angle of tail actuator 1, positive increasing thrust. | */
   ARCHER_ANGLE_TAIL2=5, /* Angle of tail actuator 2, positive increasing thrust. | */
   ARCHER_ANGLE_WING=6, /* Incidence angle of wing, positive increasing lift. | */
   ARCHER_ANGLE_ELEVATOR=7, /* Angle of elevator, positive increasing lift. | */
   ARCHER_ANGLE_RUDDER=8, /* Angle of rudder, positive turning clockwise when viewed from above. | */
   ARCHER_ANGLE_ENUM_END=9, /*  | */
} ARCHER_ANGLE;
#endif

// MAVLINK VERSION

#ifndef MAVLINK_VERSION
#define MAVLINK_VERSION 2
#endif

#if (MAVLINK_VERSION == 0)
#undef MAVLINK_VERSION
#define MAVLINK_VERSION 2
#endif

// MESSAGE DEFINITIONS
#include "./mavlink_msg_archer_heartbeat.h"
#include "./mavlink_msg_archer_full.h"
#include "./mavlink_msg_archer_battery.h"
#include "./mavlink_msg_archer_rpm.h"
#include "./mavlink_msg_archer_temperature.h"
#include "./mavlink_msg_archer_angle.h"

// base include
#include "../minimal/minimal.h"
#include "../services/services.h"

#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#if MAVLINK_THIS_XML_IDX == MAVLINK_PRIMARY_XML_IDX
# define MAVLINK_MESSAGE_INFO {MAVLINK_MESSAGE_INFO_HEARTBEAT, MAVLINK_MESSAGE_INFO_PARAM_REQUEST_READ, MAVLINK_MESSAGE_INFO_PARAM_REQUEST_LIST, MAVLINK_MESSAGE_INFO_PARAM_VALUE, MAVLINK_MESSAGE_INFO_PARAM_SET, MAVLINK_MESSAGE_INFO_PROTOCOL_VERSION, MAVLINK_MESSAGE_INFO_ARCHER_HEARTBEAT, MAVLINK_MESSAGE_INFO_ARCHER_FULL, MAVLINK_MESSAGE_INFO_ARCHER_BATTERY, MAVLINK_MESSAGE_INFO_ARCHER_RPM, MAVLINK_MESSAGE_INFO_ARCHER_TEMPERATURE, MAVLINK_MESSAGE_INFO_ARCHER_ANGLE}
# define MAVLINK_MESSAGE_NAMES {{ "ARCHER_ANGLE", 44005 }, { "ARCHER_BATTERY", 44002 }, { "ARCHER_FULL", 44001 }, { "ARCHER_HEARTBEAT", 44000 }, { "ARCHER_RPM", 44003 }, { "ARCHER_TEMPERATURE", 44004 }, { "HEARTBEAT", 0 }, { "PARAM_REQUEST_LIST", 21 }, { "PARAM_REQUEST_READ", 20 }, { "PARAM_SET", 23 }, { "PARAM_VALUE", 22 }, { "PROTOCOL_VERSION", 300 }}
# if MAVLINK_COMMAND_24BIT
#  include "../mavlink_get_info.h"
# endif
#endif

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // MAVLINK_ARCHER_H
