#pragma once
// MESSAGE ARCHER_TEMPERATURE PACKING

#define MAVLINK_MSG_ID_ARCHER_TEMPERATURE 44004

MAVPACKED(
typedef struct __mavlink_archer_temperature_t {
 int16_t temperature; /*< [cdegC] Measured temperature.*/
 uint8_t location; /*<  See the ARCHER_TEMPERATURE_LOCATION enum.*/
}) mavlink_archer_temperature_t;

#define MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN 3
#define MAVLINK_MSG_ID_ARCHER_TEMPERATURE_MIN_LEN 3
#define MAVLINK_MSG_ID_44004_LEN 3
#define MAVLINK_MSG_ID_44004_MIN_LEN 3

#define MAVLINK_MSG_ID_ARCHER_TEMPERATURE_CRC 107
#define MAVLINK_MSG_ID_44004_CRC 107



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ARCHER_TEMPERATURE { \
    44004, \
    "ARCHER_TEMPERATURE", \
    2, \
    {  { "location", NULL, MAVLINK_TYPE_UINT8_T, 0, 2, offsetof(mavlink_archer_temperature_t, location) }, \
         { "temperature", NULL, MAVLINK_TYPE_INT16_T, 0, 0, offsetof(mavlink_archer_temperature_t, temperature) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ARCHER_TEMPERATURE { \
    "ARCHER_TEMPERATURE", \
    2, \
    {  { "location", NULL, MAVLINK_TYPE_UINT8_T, 0, 2, offsetof(mavlink_archer_temperature_t, location) }, \
         { "temperature", NULL, MAVLINK_TYPE_INT16_T, 0, 0, offsetof(mavlink_archer_temperature_t, temperature) }, \
         } \
}
#endif

/**
 * @brief Pack a archer_temperature message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param location  See the ARCHER_TEMPERATURE_LOCATION enum.
 * @param temperature [cdegC] Measured temperature.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_temperature_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t location, int16_t temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN];
    _mav_put_int16_t(buf, 0, temperature);
    _mav_put_uint8_t(buf, 2, location);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN);
#else
    mavlink_archer_temperature_t packet;
    packet.temperature = temperature;
    packet.location = location;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_TEMPERATURE;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_CRC);
}

/**
 * @brief Pack a archer_temperature message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param location  See the ARCHER_TEMPERATURE_LOCATION enum.
 * @param temperature [cdegC] Measured temperature.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_temperature_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t location,int16_t temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN];
    _mav_put_int16_t(buf, 0, temperature);
    _mav_put_uint8_t(buf, 2, location);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN);
#else
    mavlink_archer_temperature_t packet;
    packet.temperature = temperature;
    packet.location = location;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_TEMPERATURE;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_CRC);
}

/**
 * @brief Encode a archer_temperature struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param archer_temperature C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_temperature_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_archer_temperature_t* archer_temperature)
{
    return mavlink_msg_archer_temperature_pack(system_id, component_id, msg, archer_temperature->location, archer_temperature->temperature);
}

/**
 * @brief Encode a archer_temperature struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param archer_temperature C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_temperature_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_archer_temperature_t* archer_temperature)
{
    return mavlink_msg_archer_temperature_pack_chan(system_id, component_id, chan, msg, archer_temperature->location, archer_temperature->temperature);
}

/**
 * @brief Send a archer_temperature message
 * @param chan MAVLink channel to send the message
 *
 * @param location  See the ARCHER_TEMPERATURE_LOCATION enum.
 * @param temperature [cdegC] Measured temperature.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_archer_temperature_send(mavlink_channel_t chan, uint8_t location, int16_t temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN];
    _mav_put_int16_t(buf, 0, temperature);
    _mav_put_uint8_t(buf, 2, location);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_TEMPERATURE, buf, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_CRC);
#else
    mavlink_archer_temperature_t packet;
    packet.temperature = temperature;
    packet.location = location;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_TEMPERATURE, (const char *)&packet, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_CRC);
#endif
}

/**
 * @brief Send a archer_temperature message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_archer_temperature_send_struct(mavlink_channel_t chan, const mavlink_archer_temperature_t* archer_temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_archer_temperature_send(chan, archer_temperature->location, archer_temperature->temperature);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_TEMPERATURE, (const char *)archer_temperature, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_CRC);
#endif
}

#if MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_archer_temperature_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t location, int16_t temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_int16_t(buf, 0, temperature);
    _mav_put_uint8_t(buf, 2, location);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_TEMPERATURE, buf, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_CRC);
#else
    mavlink_archer_temperature_t *packet = (mavlink_archer_temperature_t *)msgbuf;
    packet->temperature = temperature;
    packet->location = location;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_TEMPERATURE, (const char *)packet, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_CRC);
#endif
}
#endif

#endif

// MESSAGE ARCHER_TEMPERATURE UNPACKING


/**
 * @brief Get field location from archer_temperature message
 *
 * @return  See the ARCHER_TEMPERATURE_LOCATION enum.
 */
static inline uint8_t mavlink_msg_archer_temperature_get_location(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  2);
}

/**
 * @brief Get field temperature from archer_temperature message
 *
 * @return [cdegC] Measured temperature.
 */
static inline int16_t mavlink_msg_archer_temperature_get_temperature(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  0);
}

/**
 * @brief Decode a archer_temperature message into a struct
 *
 * @param msg The message to decode
 * @param archer_temperature C-struct to decode the message contents into
 */
static inline void mavlink_msg_archer_temperature_decode(const mavlink_message_t* msg, mavlink_archer_temperature_t* archer_temperature)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    archer_temperature->temperature = mavlink_msg_archer_temperature_get_temperature(msg);
    archer_temperature->location = mavlink_msg_archer_temperature_get_location(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN? msg->len : MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN;
        memset(archer_temperature, 0, MAVLINK_MSG_ID_ARCHER_TEMPERATURE_LEN);
    memcpy(archer_temperature, _MAV_PAYLOAD(msg), len);
#endif
}
