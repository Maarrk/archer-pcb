#pragma once
// MESSAGE ARCHER_ANGLE PACKING

#define MAVLINK_MSG_ID_ARCHER_ANGLE 44005

MAVPACKED(
typedef struct __mavlink_archer_angle_t {
 int16_t angle; /*< [cdeg] Measured angle.*/
 uint8_t location; /*<  See the ARCHER_ANGLE enum.*/
}) mavlink_archer_angle_t;

#define MAVLINK_MSG_ID_ARCHER_ANGLE_LEN 3
#define MAVLINK_MSG_ID_ARCHER_ANGLE_MIN_LEN 3
#define MAVLINK_MSG_ID_44005_LEN 3
#define MAVLINK_MSG_ID_44005_MIN_LEN 3

#define MAVLINK_MSG_ID_ARCHER_ANGLE_CRC 69
#define MAVLINK_MSG_ID_44005_CRC 69



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ARCHER_ANGLE { \
    44005, \
    "ARCHER_ANGLE", \
    2, \
    {  { "location", NULL, MAVLINK_TYPE_UINT8_T, 0, 2, offsetof(mavlink_archer_angle_t, location) }, \
         { "angle", NULL, MAVLINK_TYPE_INT16_T, 0, 0, offsetof(mavlink_archer_angle_t, angle) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ARCHER_ANGLE { \
    "ARCHER_ANGLE", \
    2, \
    {  { "location", NULL, MAVLINK_TYPE_UINT8_T, 0, 2, offsetof(mavlink_archer_angle_t, location) }, \
         { "angle", NULL, MAVLINK_TYPE_INT16_T, 0, 0, offsetof(mavlink_archer_angle_t, angle) }, \
         } \
}
#endif

/**
 * @brief Pack a archer_angle message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param location  See the ARCHER_ANGLE enum.
 * @param angle [cdeg] Measured angle.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_angle_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t location, int16_t angle)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_ANGLE_LEN];
    _mav_put_int16_t(buf, 0, angle);
    _mav_put_uint8_t(buf, 2, location);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN);
#else
    mavlink_archer_angle_t packet;
    packet.angle = angle;
    packet.location = location;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_ANGLE;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ARCHER_ANGLE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_CRC);
}

/**
 * @brief Pack a archer_angle message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param location  See the ARCHER_ANGLE enum.
 * @param angle [cdeg] Measured angle.
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_angle_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t location,int16_t angle)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_ANGLE_LEN];
    _mav_put_int16_t(buf, 0, angle);
    _mav_put_uint8_t(buf, 2, location);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN);
#else
    mavlink_archer_angle_t packet;
    packet.angle = angle;
    packet.location = location;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_ANGLE;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ARCHER_ANGLE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_CRC);
}

/**
 * @brief Encode a archer_angle struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param archer_angle C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_angle_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_archer_angle_t* archer_angle)
{
    return mavlink_msg_archer_angle_pack(system_id, component_id, msg, archer_angle->location, archer_angle->angle);
}

/**
 * @brief Encode a archer_angle struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param archer_angle C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_angle_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_archer_angle_t* archer_angle)
{
    return mavlink_msg_archer_angle_pack_chan(system_id, component_id, chan, msg, archer_angle->location, archer_angle->angle);
}

/**
 * @brief Send a archer_angle message
 * @param chan MAVLink channel to send the message
 *
 * @param location  See the ARCHER_ANGLE enum.
 * @param angle [cdeg] Measured angle.
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_archer_angle_send(mavlink_channel_t chan, uint8_t location, int16_t angle)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_ANGLE_LEN];
    _mav_put_int16_t(buf, 0, angle);
    _mav_put_uint8_t(buf, 2, location);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_ANGLE, buf, MAVLINK_MSG_ID_ARCHER_ANGLE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_CRC);
#else
    mavlink_archer_angle_t packet;
    packet.angle = angle;
    packet.location = location;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_ANGLE, (const char *)&packet, MAVLINK_MSG_ID_ARCHER_ANGLE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_CRC);
#endif
}

/**
 * @brief Send a archer_angle message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_archer_angle_send_struct(mavlink_channel_t chan, const mavlink_archer_angle_t* archer_angle)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_archer_angle_send(chan, archer_angle->location, archer_angle->angle);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_ANGLE, (const char *)archer_angle, MAVLINK_MSG_ID_ARCHER_ANGLE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_CRC);
#endif
}

#if MAVLINK_MSG_ID_ARCHER_ANGLE_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_archer_angle_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t location, int16_t angle)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_int16_t(buf, 0, angle);
    _mav_put_uint8_t(buf, 2, location);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_ANGLE, buf, MAVLINK_MSG_ID_ARCHER_ANGLE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_CRC);
#else
    mavlink_archer_angle_t *packet = (mavlink_archer_angle_t *)msgbuf;
    packet->angle = angle;
    packet->location = location;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_ANGLE, (const char *)packet, MAVLINK_MSG_ID_ARCHER_ANGLE_MIN_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN, MAVLINK_MSG_ID_ARCHER_ANGLE_CRC);
#endif
}
#endif

#endif

// MESSAGE ARCHER_ANGLE UNPACKING


/**
 * @brief Get field location from archer_angle message
 *
 * @return  See the ARCHER_ANGLE enum.
 */
static inline uint8_t mavlink_msg_archer_angle_get_location(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  2);
}

/**
 * @brief Get field angle from archer_angle message
 *
 * @return [cdeg] Measured angle.
 */
static inline int16_t mavlink_msg_archer_angle_get_angle(const mavlink_message_t* msg)
{
    return _MAV_RETURN_int16_t(msg,  0);
}

/**
 * @brief Decode a archer_angle message into a struct
 *
 * @param msg The message to decode
 * @param archer_angle C-struct to decode the message contents into
 */
static inline void mavlink_msg_archer_angle_decode(const mavlink_message_t* msg, mavlink_archer_angle_t* archer_angle)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    archer_angle->angle = mavlink_msg_archer_angle_get_angle(msg);
    archer_angle->location = mavlink_msg_archer_angle_get_location(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ARCHER_ANGLE_LEN? msg->len : MAVLINK_MSG_ID_ARCHER_ANGLE_LEN;
        memset(archer_angle, 0, MAVLINK_MSG_ID_ARCHER_ANGLE_LEN);
    memcpy(archer_angle, _MAV_PAYLOAD(msg), len);
#endif
}
