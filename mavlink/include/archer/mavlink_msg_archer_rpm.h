#pragma once
// MESSAGE ARCHER_RPM PACKING

#define MAVLINK_MSG_ID_ARCHER_RPM 44003

MAVPACKED(
typedef struct __mavlink_archer_rpm_t {
 uint16_t rpm_main; /*< [rpm] Main motor revolutions per minute*/
 uint16_t rpm_tail1; /*< [rpm] 1st tail motor revolutions per minute*/
 uint16_t rpm_tail2; /*< [rpm] 2nd tail motor revolutions per minute*/
}) mavlink_archer_rpm_t;

#define MAVLINK_MSG_ID_ARCHER_RPM_LEN 6
#define MAVLINK_MSG_ID_ARCHER_RPM_MIN_LEN 6
#define MAVLINK_MSG_ID_44003_LEN 6
#define MAVLINK_MSG_ID_44003_MIN_LEN 6

#define MAVLINK_MSG_ID_ARCHER_RPM_CRC 187
#define MAVLINK_MSG_ID_44003_CRC 187



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ARCHER_RPM { \
    44003, \
    "ARCHER_RPM", \
    3, \
    {  { "rpm_main", NULL, MAVLINK_TYPE_UINT16_T, 0, 0, offsetof(mavlink_archer_rpm_t, rpm_main) }, \
         { "rpm_tail1", NULL, MAVLINK_TYPE_UINT16_T, 0, 2, offsetof(mavlink_archer_rpm_t, rpm_tail1) }, \
         { "rpm_tail2", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_archer_rpm_t, rpm_tail2) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ARCHER_RPM { \
    "ARCHER_RPM", \
    3, \
    {  { "rpm_main", NULL, MAVLINK_TYPE_UINT16_T, 0, 0, offsetof(mavlink_archer_rpm_t, rpm_main) }, \
         { "rpm_tail1", NULL, MAVLINK_TYPE_UINT16_T, 0, 2, offsetof(mavlink_archer_rpm_t, rpm_tail1) }, \
         { "rpm_tail2", NULL, MAVLINK_TYPE_UINT16_T, 0, 4, offsetof(mavlink_archer_rpm_t, rpm_tail2) }, \
         } \
}
#endif

/**
 * @brief Pack a archer_rpm message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param rpm_main [rpm] Main motor revolutions per minute
 * @param rpm_tail1 [rpm] 1st tail motor revolutions per minute
 * @param rpm_tail2 [rpm] 2nd tail motor revolutions per minute
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_rpm_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint16_t rpm_main, uint16_t rpm_tail1, uint16_t rpm_tail2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_RPM_LEN];
    _mav_put_uint16_t(buf, 0, rpm_main);
    _mav_put_uint16_t(buf, 2, rpm_tail1);
    _mav_put_uint16_t(buf, 4, rpm_tail2);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_RPM_LEN);
#else
    mavlink_archer_rpm_t packet;
    packet.rpm_main = rpm_main;
    packet.rpm_tail1 = rpm_tail1;
    packet.rpm_tail2 = rpm_tail2;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_RPM_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_RPM;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ARCHER_RPM_MIN_LEN, MAVLINK_MSG_ID_ARCHER_RPM_LEN, MAVLINK_MSG_ID_ARCHER_RPM_CRC);
}

/**
 * @brief Pack a archer_rpm message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param rpm_main [rpm] Main motor revolutions per minute
 * @param rpm_tail1 [rpm] 1st tail motor revolutions per minute
 * @param rpm_tail2 [rpm] 2nd tail motor revolutions per minute
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_archer_rpm_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint16_t rpm_main,uint16_t rpm_tail1,uint16_t rpm_tail2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_RPM_LEN];
    _mav_put_uint16_t(buf, 0, rpm_main);
    _mav_put_uint16_t(buf, 2, rpm_tail1);
    _mav_put_uint16_t(buf, 4, rpm_tail2);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ARCHER_RPM_LEN);
#else
    mavlink_archer_rpm_t packet;
    packet.rpm_main = rpm_main;
    packet.rpm_tail1 = rpm_tail1;
    packet.rpm_tail2 = rpm_tail2;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ARCHER_RPM_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ARCHER_RPM;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ARCHER_RPM_MIN_LEN, MAVLINK_MSG_ID_ARCHER_RPM_LEN, MAVLINK_MSG_ID_ARCHER_RPM_CRC);
}

/**
 * @brief Encode a archer_rpm struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param archer_rpm C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_rpm_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_archer_rpm_t* archer_rpm)
{
    return mavlink_msg_archer_rpm_pack(system_id, component_id, msg, archer_rpm->rpm_main, archer_rpm->rpm_tail1, archer_rpm->rpm_tail2);
}

/**
 * @brief Encode a archer_rpm struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param archer_rpm C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_archer_rpm_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_archer_rpm_t* archer_rpm)
{
    return mavlink_msg_archer_rpm_pack_chan(system_id, component_id, chan, msg, archer_rpm->rpm_main, archer_rpm->rpm_tail1, archer_rpm->rpm_tail2);
}

/**
 * @brief Send a archer_rpm message
 * @param chan MAVLink channel to send the message
 *
 * @param rpm_main [rpm] Main motor revolutions per minute
 * @param rpm_tail1 [rpm] 1st tail motor revolutions per minute
 * @param rpm_tail2 [rpm] 2nd tail motor revolutions per minute
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_archer_rpm_send(mavlink_channel_t chan, uint16_t rpm_main, uint16_t rpm_tail1, uint16_t rpm_tail2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ARCHER_RPM_LEN];
    _mav_put_uint16_t(buf, 0, rpm_main);
    _mav_put_uint16_t(buf, 2, rpm_tail1);
    _mav_put_uint16_t(buf, 4, rpm_tail2);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_RPM, buf, MAVLINK_MSG_ID_ARCHER_RPM_MIN_LEN, MAVLINK_MSG_ID_ARCHER_RPM_LEN, MAVLINK_MSG_ID_ARCHER_RPM_CRC);
#else
    mavlink_archer_rpm_t packet;
    packet.rpm_main = rpm_main;
    packet.rpm_tail1 = rpm_tail1;
    packet.rpm_tail2 = rpm_tail2;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_RPM, (const char *)&packet, MAVLINK_MSG_ID_ARCHER_RPM_MIN_LEN, MAVLINK_MSG_ID_ARCHER_RPM_LEN, MAVLINK_MSG_ID_ARCHER_RPM_CRC);
#endif
}

/**
 * @brief Send a archer_rpm message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_archer_rpm_send_struct(mavlink_channel_t chan, const mavlink_archer_rpm_t* archer_rpm)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_archer_rpm_send(chan, archer_rpm->rpm_main, archer_rpm->rpm_tail1, archer_rpm->rpm_tail2);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_RPM, (const char *)archer_rpm, MAVLINK_MSG_ID_ARCHER_RPM_MIN_LEN, MAVLINK_MSG_ID_ARCHER_RPM_LEN, MAVLINK_MSG_ID_ARCHER_RPM_CRC);
#endif
}

#if MAVLINK_MSG_ID_ARCHER_RPM_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_archer_rpm_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint16_t rpm_main, uint16_t rpm_tail1, uint16_t rpm_tail2)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint16_t(buf, 0, rpm_main);
    _mav_put_uint16_t(buf, 2, rpm_tail1);
    _mav_put_uint16_t(buf, 4, rpm_tail2);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_RPM, buf, MAVLINK_MSG_ID_ARCHER_RPM_MIN_LEN, MAVLINK_MSG_ID_ARCHER_RPM_LEN, MAVLINK_MSG_ID_ARCHER_RPM_CRC);
#else
    mavlink_archer_rpm_t *packet = (mavlink_archer_rpm_t *)msgbuf;
    packet->rpm_main = rpm_main;
    packet->rpm_tail1 = rpm_tail1;
    packet->rpm_tail2 = rpm_tail2;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ARCHER_RPM, (const char *)packet, MAVLINK_MSG_ID_ARCHER_RPM_MIN_LEN, MAVLINK_MSG_ID_ARCHER_RPM_LEN, MAVLINK_MSG_ID_ARCHER_RPM_CRC);
#endif
}
#endif

#endif

// MESSAGE ARCHER_RPM UNPACKING


/**
 * @brief Get field rpm_main from archer_rpm message
 *
 * @return [rpm] Main motor revolutions per minute
 */
static inline uint16_t mavlink_msg_archer_rpm_get_rpm_main(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  0);
}

/**
 * @brief Get field rpm_tail1 from archer_rpm message
 *
 * @return [rpm] 1st tail motor revolutions per minute
 */
static inline uint16_t mavlink_msg_archer_rpm_get_rpm_tail1(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  2);
}

/**
 * @brief Get field rpm_tail2 from archer_rpm message
 *
 * @return [rpm] 2nd tail motor revolutions per minute
 */
static inline uint16_t mavlink_msg_archer_rpm_get_rpm_tail2(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint16_t(msg,  4);
}

/**
 * @brief Decode a archer_rpm message into a struct
 *
 * @param msg The message to decode
 * @param archer_rpm C-struct to decode the message contents into
 */
static inline void mavlink_msg_archer_rpm_decode(const mavlink_message_t* msg, mavlink_archer_rpm_t* archer_rpm)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    archer_rpm->rpm_main = mavlink_msg_archer_rpm_get_rpm_main(msg);
    archer_rpm->rpm_tail1 = mavlink_msg_archer_rpm_get_rpm_tail1(msg);
    archer_rpm->rpm_tail2 = mavlink_msg_archer_rpm_get_rpm_tail2(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ARCHER_RPM_LEN? msg->len : MAVLINK_MSG_ID_ARCHER_RPM_LEN;
        memset(archer_rpm, 0, MAVLINK_MSG_ID_ARCHER_RPM_LEN);
    memcpy(archer_rpm, _MAV_PAYLOAD(msg), len);
#endif
}
