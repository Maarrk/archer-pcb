#!/usr/bin/env python3
import time
import argparse
import serial
import os
from datetime import datetime
from pymavlink.dialects.v20 import archer

parser = argparse.ArgumentParser(description='Read incoming MAVLink in ARCHER dialect on a serial device.')
parser.add_argument('-d', '--device', default='/dev/ttyUSB0', help='Select input device (default: %(default)s)')
args = parser.parse_args()

logs_dir = 'logs'
root_dir = os.path.dirname(os.path.abspath(__file__))
if not os.path.exists(os.path.join(root_dir, logs_dir)):
    os.makedirs(os.path.join(root_dir, logs_dir), 0o777)

filename = 'archer-serial-reader-output-' + datetime.now().strftime('%Y%m%dT%H%M%S.bin')

ser = serial.Serial(args.device, 57600)
mav = archer.MAVLink(open(os.path.join(root_dir, logs_dir, filename), 'wb'))

while True:
    try:
        data = ser.read_all()
        if len(data) > 0:
            print(' '.join('{:02x}'.format(b) for b in data))
        msgs = mav.parse_buffer(data)
        if msgs is not None and len(msgs) > 0:
            for msg in msgs:
                print(msg)
                mav.send(msg)
    except:
        print('Error occured')
    time.sleep(0.05)
