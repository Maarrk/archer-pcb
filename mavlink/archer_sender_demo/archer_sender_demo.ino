#include "include/archer/mavlink.h"

#define MAV_SYSTEM_ID 1
#define MAV_COMP_ID_PERIPHERAL 158
#define MAVLINK_OVERHEAD_V2_UNSIGNED 12

mavlink_message_t msg;
char buffer[MAVLINK_MAX_DIALECT_PAYLOAD_SIZE + MAVLINK_OVERHEAD_V2_UNSIGNED] = {0};
uint16_t msg_len;

// Note: over the wire format is little endian
// Before payload are E1 AB 00 which encode 44001
// Indices in message are given below, counting from 0xFD (start byte) at index 0
uint16_t voltage = 22200; // low 10, high 11
uint16_t current = 3700; // low 12, high 13
uint16_t rpm_main = 1200; // low 14, high 15

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  
  Serial.begin(57600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void loop() {
  mavlink_msg_archer_full_pack(
    MAV_SYSTEM_ID,
    MAV_COMP_ID_PERIPHERAL,
    &msg,
    voltage,
    current,
    rpm_main
  );
  msg_len = mavlink_msg_to_send_buffer(buffer, &msg);
  Serial.write(buffer, msg_len);
  
  digitalWrite(LED_BUILTIN, HIGH);
  delay(100);
  digitalWrite(LED_BUILTIN, LOW);
  delay(900);
}
