EESchema Schematic File Version 4
LIBS:archer-power-board-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "ARCHER Powerboard"
Date "24.11.2019"
Rev "1.1"
Comp "Warsaw University of Technology"
Comment1 "Marek Łukasiewicz"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 9850 3800 2    50   Output ~ 0
UART_TX
Text HLabel 9250 3650 2    50   Input ~ 0
UART_RX
Text HLabel 1000 3900 0    50   Input ~ 0
VIN_I_MAIN
Text HLabel 1000 5150 0    50   Input ~ 0
VIN_I_TAIL1
Text HLabel 1000 6400 0    50   Input ~ 0
VIN_I_TAIL2
Text HLabel 3000 3900 0    50   Input ~ 0
VIN_V_BATT
$Comp
L Regulator_Linear:L78L33_SOT89 U6
U 1 1 5D5E8B53
P 1550 900
F 0 "U6" H 1550 1142 50  0000 C CNN
F 1 "L78L33_SOT89" H 1550 1051 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-89-3_Handsoldering" H 1550 1100 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/15/55/e5/aa/23/5b/43/fd/CD00000446.pdf/files/CD00000446.pdf/jcr:content/translations/en.CD00000446.pdf" H 1550 850 50  0001 C CNN
	1    1550 900 
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR036
U 1 1 5D5E8C71
P 550 900
F 0 "#PWR036" H 550 750 50  0001 C CNN
F 1 "+5V" H 565 1073 50  0000 C CNN
F 2 "" H 550 900 50  0001 C CNN
F 3 "" H 550 900 50  0001 C CNN
	1    550  900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR045
U 1 1 5D5EB00E
P 1550 1200
F 0 "#PWR045" H 1550 950 50  0001 C CNN
F 1 "GND" H 1555 1027 50  0000 C CNN
F 2 "" H 1550 1200 50  0001 C CNN
F 3 "" H 1550 1200 50  0001 C CNN
	1    1550 1200
	1    0    0    -1  
$EndComp
$Comp
L Device:C C15
U 1 1 5D5EC2B5
P 2300 1200
F 0 "C15" H 2415 1246 50  0000 L CNN
F 1 "100nF" H 2415 1155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2338 1050 50  0001 C CNN
F 3 "~" H 2300 1200 50  0001 C CNN
	1    2300 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR052
U 1 1 5D5ECB7C
P 2300 1350
F 0 "#PWR052" H 2300 1100 50  0001 C CNN
F 1 "GND" H 2305 1177 50  0000 C CNN
F 2 "" H 2300 1350 50  0001 C CNN
F 3 "" H 2300 1350 50  0001 C CNN
	1    2300 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D5
U 1 1 5D5ED02F
P 2850 1600
F 0 "D5" V 2889 1482 50  0000 R CNN
F 1 " LED_RED" V 2798 1482 50  0000 R CNN
F 2 "LEDs:LED_1206_HandSoldering" H 2850 1600 50  0001 C CNN
F 3 "~" H 2850 1600 50  0001 C CNN
	1    2850 1600
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R16
U 1 1 5D5EE4C5
P 2850 1200
F 0 "R16" H 2920 1246 50  0000 L CNN
F 1 "510" H 2920 1155 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2780 1200 50  0001 C CNN
F 3 "~" H 2850 1200 50  0001 C CNN
	1    2850 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 1350 2850 1450
$Comp
L power:GND #PWR054
U 1 1 5D5F3CA3
P 2850 1750
F 0 "#PWR054" H 2850 1500 50  0001 C CNN
F 1 "GND" H 2855 1577 50  0000 C CNN
F 2 "" H 2850 1750 50  0001 C CNN
F 3 "" H 2850 1750 50  0001 C CNN
	1    2850 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 900  2050 900 
Connection ~ 2300 900 
Wire Wire Line
	2300 900  2300 1050
Wire Wire Line
	2300 900  2850 900 
Wire Wire Line
	2850 900  2850 1050
Connection ~ 2850 900 
Wire Wire Line
	2850 900  3200 900 
$Comp
L Device:LED D9
U 1 1 5D5F6BB0
P 10450 3350
F 0 "D9" V 10489 3232 50  0000 R CNN
F 1 " LED_GREEN" V 10398 3232 50  0000 R CNN
F 2 "LEDs:LED_1206_HandSoldering" H 10450 3350 50  0001 C CNN
F 3 "~" H 10450 3350 50  0001 C CNN
	1    10450 3350
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R26
U 1 1 5D5F75EB
P 10450 3000
F 0 "R26" H 10520 3046 50  0000 L CNN
F 1 "510" H 10520 2955 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 10380 3000 50  0001 C CNN
F 3 "~" H 10450 3000 50  0001 C CNN
	1    10450 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 3150 10450 3200
$Comp
L power:GND #PWR081
U 1 1 5D5F80AC
P 10450 3600
F 0 "#PWR081" H 10450 3350 50  0001 C CNN
F 1 "GND" H 10455 3427 50  0000 C CNN
F 2 "" H 10450 3600 50  0001 C CNN
F 3 "" H 10450 3600 50  0001 C CNN
	1    10450 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 3500 10450 3600
Wire Notes Line
	11100 3900 10350 3900
Wire Notes Line
	10350 3900 10350 2700
Wire Notes Line
	10350 2700 11100 2700
Wire Notes Line
	11100 2700 11100 3900
Text Notes 10350 3900 0    50   ~ 0
User LED
Wire Notes Line
	2700 2050 2700 1000
Wire Notes Line
	2700 1000 3350 1000
Wire Notes Line
	3350 1000 3350 2050
Wire Notes Line
	3350 2050 2700 2050
Text Notes 2700 2050 0    50   ~ 0
3.3V indicator
Wire Notes Line
	2650 2050 2650 550 
Wire Notes Line
	2650 550  500  550 
Wire Notes Line
	500  550  500  2050
Wire Notes Line
	500  2050 2650 2050
Text Notes 500  2050 0    50   ~ 0
3.3V power supply
Wire Wire Line
	3100 4400 3100 4500
Connection ~ 3100 4400
Text GLabel 3800 4400 2    50   Input ~ 0
ADC_V_BATT
Wire Wire Line
	3100 4300 3100 4400
$Comp
L power:GND #PWR056
U 1 1 5D60D111
P 3100 4800
F 0 "#PWR056" H 3100 4550 50  0001 C CNN
F 1 "GND" H 3105 4627 50  0000 C CNN
F 2 "" H 3100 4800 50  0001 C CNN
F 3 "" H 3100 4800 50  0001 C CNN
	1    3100 4800
	1    0    0    -1  
$EndComp
$Comp
L Device:R R19
U 1 1 5D60C8F4
P 3100 4650
F 0 "R19" H 3170 4696 50  0000 L CNN
F 1 "10k" H 3170 4605 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3030 4650 50  0001 C CNN
F 3 "~" H 3100 4650 50  0001 C CNN
	1    3100 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3900 3100 4000
Wire Wire Line
	3000 3900 3100 3900
$Comp
L Device:R R18
U 1 1 5D60BD69
P 3100 4150
F 0 "R18" H 3170 4196 50  0000 L CNN
F 1 "82k" H 3170 4105 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3030 4150 50  0001 C CNN
F 3 "~" H 3100 4150 50  0001 C CNN
	1    3100 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5D607FAA
P 1100 4150
F 0 "R7" H 1170 4196 50  0000 L CNN
F 1 "2k2" H 1170 4105 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1030 4150 50  0001 C CNN
F 3 "~" H 1100 4150 50  0001 C CNN
	1    1100 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5D608754
P 1100 4650
F 0 "R8" H 1170 4696 50  0000 L CNN
F 1 "3k3" H 1170 4605 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1030 4650 50  0001 C CNN
F 3 "~" H 1100 4650 50  0001 C CNN
	1    1100 4650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR042
U 1 1 5D608BED
P 1100 4800
F 0 "#PWR042" H 1100 4550 50  0001 C CNN
F 1 "GND" H 1105 4627 50  0000 C CNN
F 2 "" H 1100 4800 50  0001 C CNN
F 3 "" H 1100 4800 50  0001 C CNN
	1    1100 4800
	1    0    0    -1  
$EndComp
Text GLabel 1800 4400 2    50   Input ~ 0
ADC_I_MAIN
Wire Wire Line
	1000 3900 1100 3900
Wire Wire Line
	1100 3900 1100 4000
$Comp
L Device:R R9
U 1 1 5D60A44D
P 1100 5400
F 0 "R9" H 1170 5446 50  0000 L CNN
F 1 "2k2" H 1170 5355 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1030 5400 50  0001 C CNN
F 3 "~" H 1100 5400 50  0001 C CNN
	1    1100 5400
	1    0    0    -1  
$EndComp
Text GLabel 1800 5650 2    50   Input ~ 0
ADC_I_TAIL1
Wire Wire Line
	1000 5150 1100 5150
Wire Wire Line
	1100 5150 1100 5250
$Comp
L Device:R R11
U 1 1 5D60C7D2
P 1100 6650
F 0 "R11" H 1170 6696 50  0000 L CNN
F 1 "2k2" H 1170 6605 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1030 6650 50  0001 C CNN
F 3 "~" H 1100 6650 50  0001 C CNN
	1    1100 6650
	1    0    0    -1  
$EndComp
Text GLabel 1800 6900 2    50   Input ~ 0
ADC_I_TAIL2
Wire Wire Line
	1000 6400 1100 6400
Wire Wire Line
	1100 6400 1100 6500
$Comp
L power:GND #PWR044
U 1 1 5D60CE78
P 1100 7300
F 0 "#PWR044" H 1100 7050 50  0001 C CNN
F 1 "GND" H 1105 7127 50  0000 C CNN
F 2 "" H 1100 7300 50  0001 C CNN
F 3 "" H 1100 7300 50  0001 C CNN
	1    1100 7300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R12
U 1 1 5D60CB22
P 1100 7150
F 0 "R12" H 1170 7196 50  0000 L CNN
F 1 "3k3" H 1170 7105 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1030 7150 50  0001 C CNN
F 3 "~" H 1100 7150 50  0001 C CNN
	1    1100 7150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR043
U 1 1 5D60AC01
P 1100 6050
F 0 "#PWR043" H 1100 5800 50  0001 C CNN
F 1 "GND" H 1105 5877 50  0000 C CNN
F 2 "" H 1100 6050 50  0001 C CNN
F 3 "" H 1100 6050 50  0001 C CNN
	1    1100 6050
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5D60A917
P 1100 5900
F 0 "R10" H 1170 5946 50  0000 L CNN
F 1 "3k3" H 1170 5855 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1030 5900 50  0001 C CNN
F 3 "~" H 1100 5900 50  0001 C CNN
	1    1100 5900
	1    0    0    -1  
$EndComp
Wire Notes Line
	500  3800 2400 3800
Wire Notes Line
	500  7600 500  3800
Text Notes 500  7600 0    50   ~ 0
Hall sensor dividers
Wire Notes Line
	2500 3800 4350 3800
Wire Notes Line
	4350 3800 4350 5050
Wire Notes Line
	4350 5050 2500 5050
Wire Notes Line
	2500 5050 2500 3800
Text Notes 2500 5050 0    50   ~ 0
6S divider
$Comp
L Device:LED D6
U 1 1 5D60D625
P 2850 3250
F 0 "D6" V 2889 3132 50  0000 R CNN
F 1 " LED_ORANGE" V 2798 3132 50  0000 R CNN
F 2 "LEDs:LED_1206_HandSoldering" H 2850 3250 50  0001 C CNN
F 3 "~" H 2850 3250 50  0001 C CNN
	1    2850 3250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R17
U 1 1 5D60D62F
P 2850 2850
F 0 "R17" H 2920 2896 50  0000 L CNN
F 1 "510" H 2920 2805 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2780 2850 50  0001 C CNN
F 3 "~" H 2850 2850 50  0001 C CNN
	1    2850 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2850 3000 2850 3100
Wire Wire Line
	2850 2550 2850 2700
Wire Wire Line
	2850 2550 3200 2550
Wire Notes Line
	2700 3700 2700 2650
Wire Notes Line
	2700 2650 3350 2650
Wire Notes Line
	3350 2650 3350 3700
Wire Notes Line
	3350 3700 2700 3700
Text Notes 2700 3700 0    50   ~ 0
3.3VA indicator
$Comp
L power:+3.3VA #PWR058
U 1 1 5D61560F
P 3200 2550
F 0 "#PWR058" H 3200 2400 50  0001 C CNN
F 1 "+3.3VA" H 3215 2723 50  0000 C CNN
F 2 "" H 3200 2550 50  0001 C CNN
F 3 "" H 3200 2550 50  0001 C CNN
	1    3200 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3VA #PWR072
U 1 1 5D617167
P 6750 1900
F 0 "#PWR072" H 6750 1750 50  0001 C CNN
F 1 "+3.3VA" H 6765 2073 50  0000 C CNN
F 2 "" H 6750 1900 50  0001 C CNN
F 3 "" H 6750 1900 50  0001 C CNN
	1    6750 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5D6194CE
P 10200 1200
F 0 "Y1" V 10246 1069 50  0000 R CNN
F 1 "8MHz 12pF" V 10155 1069 50  0000 R CNN
F 2 "Crystals:Crystal_HC49-4H_Vertical" H 10200 1200 50  0001 C CNN
F 3 "~" H 10200 1200 50  0001 C CNN
	1    10200 1200
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR057
U 1 1 5D5F401E
P 3200 900
F 0 "#PWR057" H 3200 750 50  0001 C CNN
F 1 "+3.3V" H 3215 1073 50  0000 C CNN
F 2 "" H 3200 900 50  0001 C CNN
F 3 "" H 3200 900 50  0001 C CNN
	1    3200 900 
	1    0    0    -1  
$EndComp
$Comp
L Device:C C26
U 1 1 5D61461F
P 5850 2050
F 0 "C26" H 5965 2096 50  0000 L CNN
F 1 "100nF" H 5965 2005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5888 1900 50  0001 C CNN
F 3 "~" H 5850 2050 50  0001 C CNN
	1    5850 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C25
U 1 1 5D639FE2
P 5400 2050
F 0 "C25" H 5515 2096 50  0000 L CNN
F 1 "100nF" H 5515 2005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 5438 1900 50  0001 C CNN
F 3 "~" H 5400 2050 50  0001 C CNN
	1    5400 2050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR069
U 1 1 5D63A000
P 5850 2200
F 0 "#PWR069" H 5850 1950 50  0001 C CNN
F 1 "GND" H 5855 2027 50  0000 C CNN
F 2 "" H 5850 2200 50  0001 C CNN
F 3 "" H 5850 2200 50  0001 C CNN
	1    5850 2200
	1    0    0    -1  
$EndComp
Text Notes 3600 1750 0    50   Italic 0
Decoupling capacitors:\n- 100nF ceramic\n- 1uF, 10uF Tantalum or ceramic
$Comp
L power:+3.3V #PWR070
U 1 1 5D692C2F
P 6250 1900
F 0 "#PWR070" H 6250 1750 50  0001 C CNN
F 1 "+3.3V" H 6265 2073 50  0000 C CNN
F 2 "" H 6250 1900 50  0001 C CNN
F 3 "" H 6250 1900 50  0001 C CNN
	1    6250 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C29
U 1 1 5D6ABF59
P 9950 1450
F 0 "C29" H 10065 1496 50  0000 L CNN
F 1 "20pF" H 10065 1405 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 9988 1300 50  0001 C CNN
F 3 "~" H 9950 1450 50  0001 C CNN
	1    9950 1450
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9950 1200 10050 1200
$Comp
L power:GND #PWR079
U 1 1 5D6B0317
P 9950 1600
F 0 "#PWR079" H 9950 1350 50  0001 C CNN
F 1 "GND" H 9955 1427 50  0000 C CNN
F 2 "" H 9950 1600 50  0001 C CNN
F 3 "" H 9950 1600 50  0001 C CNN
	1    9950 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C30
U 1 1 5D6B0669
P 10450 1450
F 0 "C30" H 10565 1496 50  0000 L CNN
F 1 "20pF" H 10565 1405 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 10488 1300 50  0001 C CNN
F 3 "~" H 10450 1450 50  0001 C CNN
	1    10450 1450
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR080
U 1 1 5D6B0B12
P 10450 1600
F 0 "#PWR080" H 10450 1350 50  0001 C CNN
F 1 "GND" H 10455 1427 50  0000 C CNN
F 2 "" H 10450 1600 50  0001 C CNN
F 3 "" H 10450 1600 50  0001 C CNN
	1    10450 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 1200 10450 1200
Text GLabel 10450 1100 2    50   Input ~ 0
MCU_OSC32_IN
Text GLabel 9950 900  2    50   Input ~ 0
MCU_OSC32_OUT
Connection ~ 9950 1200
Wire Wire Line
	9950 1200 9950 1300
Wire Wire Line
	10450 1100 10450 1200
Connection ~ 10450 1200
Wire Wire Line
	10450 1200 10450 1300
Wire Notes Line
	9600 600  11100 600 
Wire Notes Line
	11100 600  11100 2600
Wire Notes Line
	11100 2600 9600 2600
Wire Notes Line
	9600 2600 9600 600 
Text Notes 9600 2600 0    50   ~ 0
Crystal external LSE
Wire Wire Line
	9950 900  9950 1200
Text GLabel 9800 5700 0    50   Input ~ 0
NRST
Text Notes 8850 6450 0    50   ~ 0
ST Link V2 (SWD) Connection
Wire Wire Line
	5750 4800 5650 4800
Text GLabel 10650 2850 2    50   Input ~ 0
MCU_LED
Wire Wire Line
	10650 2850 10450 2850
Text GLabel 7250 3600 2    50   Input ~ 0
MCU_RX
Wire Wire Line
	7150 3500 7250 3500
Wire Wire Line
	7250 3600 7150 3600
$Comp
L Device:R R24
U 1 1 5D79A287
P 9150 3100
F 0 "R24" H 9220 3146 50  0000 L CNN
F 1 "510" H 9220 3055 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 9080 3100 50  0001 C CNN
F 3 "~" H 9150 3100 50  0001 C CNN
	1    9150 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D7
U 1 1 5D79A45A
P 9150 3400
F 0 "D7" V 9189 3282 50  0000 R CNN
F 1 " LED_BLUE" V 9098 3282 50  0000 R CNN
F 2 "LEDs:LED_1206_HandSoldering" H 9150 3400 50  0001 C CNN
F 3 "~" H 9150 3400 50  0001 C CNN
	1    9150 3400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9150 3650 9150 3550
Wire Notes Line
	10300 2700 10300 3900
Wire Notes Line
	10300 3900 8700 3900
Wire Notes Line
	8700 3900 8700 2700
Wire Notes Line
	8700 2700 10300 2700
Text Notes 8700 3900 0    50   ~ 0
UART LEDs
Text HLabel 7250 5000 2    50   BiDi ~ 0
I2C_SDA
Text HLabel 7250 4900 2    50   BiDi ~ 0
I2C_SCL
Text Notes 2500 7600 0    50   Italic 0
Halls require 4k7 or greater load resistance,\nADC for 6MHz clock can have up to 16k2 source,\nRC low pass filters cutoff circa 160Hz,\nServo and avionics current signal is already <3V
$Comp
L Device:C C13
U 1 1 5D85360F
P 2050 1200
F 0 "C13" H 2165 1246 50  0000 L CNN
F 1 "1uF" H 2165 1155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2088 1050 50  0001 C CNN
F 3 "~" H 2050 1200 50  0001 C CNN
	1    2050 1200
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR050
U 1 1 5D853D83
P 2050 1350
F 0 "#PWR050" H 2050 1100 50  0001 C CNN
F 1 "GND" H 2055 1177 50  0000 C CNN
F 2 "" H 2050 1350 50  0001 C CNN
F 3 "" H 2050 1350 50  0001 C CNN
	1    2050 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 1050 2050 900 
Connection ~ 2050 900 
Wire Wire Line
	2050 900  2300 900 
Wire Wire Line
	550  900  800  900 
$Comp
L Device:C C8
U 1 1 5D85E640
P 1050 1200
F 0 "C8" H 1165 1246 50  0000 L CNN
F 1 "100nF" H 1165 1155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1088 1050 50  0001 C CNN
F 3 "~" H 1050 1200 50  0001 C CNN
	1    1050 1200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR040
U 1 1 5D85E64A
P 1050 1350
F 0 "#PWR040" H 1050 1100 50  0001 C CNN
F 1 "GND" H 1055 1177 50  0000 C CNN
F 2 "" H 1050 1350 50  0001 C CNN
F 3 "" H 1050 1350 50  0001 C CNN
	1    1050 1350
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5D85E654
P 800 1200
F 0 "C6" H 915 1246 50  0000 L CNN
F 1 "1uF" H 915 1155 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 838 1050 50  0001 C CNN
F 3 "~" H 800 1200 50  0001 C CNN
	1    800  1200
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR038
U 1 1 5D85E65E
P 800 1350
F 0 "#PWR038" H 800 1100 50  0001 C CNN
F 1 "GND" H 805 1177 50  0000 C CNN
F 2 "" H 800 1350 50  0001 C CNN
F 3 "" H 800 1350 50  0001 C CNN
	1    800  1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 1050 1050 900 
Connection ~ 1050 900 
Wire Wire Line
	1050 900  1250 900 
Wire Wire Line
	800  1050 800  900 
Connection ~ 800  900 
Wire Wire Line
	800  900  1050 900 
$Comp
L Regulator_Linear:L78L33_SOT89 U7
U 1 1 5D88F96C
P 1550 2550
F 0 "U7" H 1550 2792 50  0000 C CNN
F 1 "L78L33_SOT89" H 1550 2701 50  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-89-3_Handsoldering" H 1550 2750 50  0001 C CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/15/55/e5/aa/23/5b/43/fd/CD00000446.pdf/files/CD00000446.pdf/jcr:content/translations/en.CD00000446.pdf" H 1550 2500 50  0001 C CNN
	1    1550 2550
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR037
U 1 1 5D88F976
P 550 2550
F 0 "#PWR037" H 550 2400 50  0001 C CNN
F 1 "+5V" H 565 2723 50  0000 C CNN
F 2 "" H 550 2550 50  0001 C CNN
F 3 "" H 550 2550 50  0001 C CNN
	1    550  2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C16
U 1 1 5D88F98A
P 2300 2850
F 0 "C16" H 2415 2896 50  0000 L CNN
F 1 "100nF" H 2415 2805 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2338 2700 50  0001 C CNN
F 3 "~" H 2300 2850 50  0001 C CNN
	1    2300 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2550 2050 2550
Connection ~ 2300 2550
Wire Wire Line
	2300 2550 2300 2700
Wire Wire Line
	2300 2550 2850 2550
Wire Notes Line
	2650 3700 2650 2200
Wire Notes Line
	2650 2200 500  2200
Wire Notes Line
	500  2200 500  3700
Wire Notes Line
	500  3700 2650 3700
Text Notes 500  3700 0    50   ~ 0
3.3V Analog power supply
$Comp
L Device:C C14
U 1 1 5D88F9A7
P 2050 2850
F 0 "C14" H 2165 2896 50  0000 L CNN
F 1 "1uF" H 2165 2805 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 2088 2700 50  0001 C CNN
F 3 "~" H 2050 2850 50  0001 C CNN
	1    2050 2850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2050 2700 2050 2550
Connection ~ 2050 2550
Wire Wire Line
	2050 2550 2300 2550
Wire Wire Line
	550  2550 800  2550
$Comp
L Device:C C9
U 1 1 5D88F9BF
P 1050 2850
F 0 "C9" H 1165 2896 50  0000 L CNN
F 1 "100nF" H 1165 2805 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 1088 2700 50  0001 C CNN
F 3 "~" H 1050 2850 50  0001 C CNN
	1    1050 2850
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR041
U 1 1 5D88F9C9
P 1050 3000
F 0 "#PWR041" H 1050 2750 50  0001 C CNN
F 1 "GND" H 1055 2827 50  0000 C CNN
F 2 "" H 1050 3000 50  0001 C CNN
F 3 "" H 1050 3000 50  0001 C CNN
	1    1050 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5D88F9D3
P 800 2850
F 0 "C7" H 915 2896 50  0000 L CNN
F 1 "1uF" H 915 2805 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 838 2700 50  0001 C CNN
F 3 "~" H 800 2850 50  0001 C CNN
	1    800  2850
	-1   0    0    -1  
$EndComp
$Comp
L power:GND #PWR039
U 1 1 5D88F9DD
P 800 3000
F 0 "#PWR039" H 800 2750 50  0001 C CNN
F 1 "GND" H 805 2827 50  0000 C CNN
F 2 "" H 800 3000 50  0001 C CNN
F 3 "" H 800 3000 50  0001 C CNN
	1    800  3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1050 2700 1050 2550
Connection ~ 1050 2550
Wire Wire Line
	1050 2550 1250 2550
Wire Wire Line
	800  2700 800  2550
Connection ~ 800  2550
Wire Wire Line
	800  2550 1050 2550
Connection ~ 2850 2550
Text GLabel 3800 5250 2    50   Input ~ 0
ADC_I_SERVO
Text GLabel 3800 6050 2    50   Input ~ 0
ADC_I_AVIO
$Comp
L power:+3.3V #PWR075
U 1 1 5DD44717
P 9150 2950
F 0 "#PWR075" H 9150 2800 50  0001 C CNN
F 1 "+3.3V" H 9165 3123 50  0000 C CNN
F 2 "" H 9150 2950 50  0001 C CNN
F 3 "" H 9150 2950 50  0001 C CNN
	1    9150 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9050 3650 9150 3650
Connection ~ 9150 3650
Wire Wire Line
	9150 3650 9250 3650
$Comp
L Device:R R25
U 1 1 5DD6BCAB
P 9750 3250
F 0 "R25" H 9820 3296 50  0000 L CNN
F 1 "510" H 9820 3205 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 9680 3250 50  0001 C CNN
F 3 "~" H 9750 3250 50  0001 C CNN
	1    9750 3250
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D8
U 1 1 5DD6BCB5
P 9750 3550
F 0 "D8" V 9789 3432 50  0000 R CNN
F 1 " LED_BLUE" V 9698 3432 50  0000 R CNN
F 2 "LEDs:LED_1206_HandSoldering" H 9750 3550 50  0001 C CNN
F 3 "~" H 9750 3550 50  0001 C CNN
	1    9750 3550
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9750 3800 9750 3700
$Comp
L power:+3.3V #PWR077
U 1 1 5DD6BCC0
P 9750 3100
F 0 "#PWR077" H 9750 2950 50  0001 C CNN
F 1 "+3.3V" H 9765 3273 50  0000 C CNN
F 2 "" H 9750 3100 50  0001 C CNN
F 3 "" H 9750 3100 50  0001 C CNN
	1    9750 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 3800 9750 3800
Connection ~ 9750 3800
Wire Wire Line
	9750 3800 9850 3800
Text GLabel 9650 3800 0    50   Input ~ 0
MCU_TX
Text GLabel 5650 4800 0    50   Input ~ 0
MCU_LED
Text GLabel 9050 3650 0    50   Input ~ 0
MCU_RX
Text GLabel 7250 3500 2    50   Input ~ 0
MCU_TX
$Comp
L power:GND #PWR076
U 1 1 5DD2C243
P 9550 5800
F 0 "#PWR076" H 9550 5550 50  0001 C CNN
F 1 "GND" H 9555 5627 50  0000 C CNN
F 2 "" H 9550 5800 50  0001 C CNN
F 3 "" H 9550 5800 50  0001 C CNN
	1    9550 5800
	0    1    1    0   
$EndComp
$Comp
L power:+5V #PWR078
U 1 1 5DD2E322
P 9800 6100
F 0 "#PWR078" H 9800 5950 50  0001 C CNN
F 1 "+5V" H 9815 6273 50  0000 C CNN
F 2 "" H 9800 6100 50  0001 C CNN
F 3 "" H 9800 6100 50  0001 C CNN
	1    9800 6100
	0    -1   -1   0   
$EndComp
Connection ~ 5300 2800
Wire Wire Line
	5100 2800 5300 2800
$Comp
L Connector_Generic:Conn_01x01 J19
U 1 1 5D7E2B49
P 5100 3000
F 0 "J19" H 5200 2900 50  0000 L CNN
F 1 "BOOT0" H 5200 3000 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 5100 3000 50  0001 C CNN
F 3 "~" H 5100 3000 50  0001 C CNN
	1    5100 3000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR067
U 1 1 5D7E23DE
P 5300 3100
F 0 "#PWR067" H 5300 2850 50  0001 C CNN
F 1 "GND" H 5305 2927 50  0000 C CNN
F 2 "" H 5300 3100 50  0001 C CNN
F 3 "" H 5300 3100 50  0001 C CNN
	1    5300 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R23
U 1 1 5D7D7A7B
P 5300 2950
F 0 "R23" H 5370 2996 50  0000 L CNN
F 1 "10k" H 5370 2905 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 5230 2950 50  0001 C CNN
F 3 "~" H 5300 2950 50  0001 C CNN
	1    5300 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5750 5800 5600 5800
Wire Wire Line
	5600 5700 5750 5700
Connection ~ 4650 2600
Wire Wire Line
	4550 2600 4650 2600
Text GLabel 4550 2600 0    50   Input ~ 0
NRST
Text GLabel 5600 5800 0    50   Input ~ 0
MCU_OSC32_OUT
Text GLabel 5600 5700 0    50   Input ~ 0
MCU_OSC32_IN
$Comp
L power:GND #PWR065
U 1 1 5D69B209
P 4650 2900
F 0 "#PWR065" H 4650 2650 50  0001 C CNN
F 1 "GND" H 4655 2727 50  0000 C CNN
F 2 "" H 4650 2900 50  0001 C CNN
F 3 "" H 4650 2900 50  0001 C CNN
	1    4650 2900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C23
U 1 1 5D69AE43
P 4650 2750
F 0 "C23" H 4765 2796 50  0000 L CNN
F 1 "100nF" H 4765 2705 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4688 2600 50  0001 C CNN
F 3 "~" H 4650 2750 50  0001 C CNN
	1    4650 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2600 5750 2600
Wire Wire Line
	5300 2800 5750 2800
Text GLabel 10500 5700 2    50   Input ~ 0
SWDIO
Text GLabel 10500 5900 2    50   Input ~ 0
SWCLK
Wire Wire Line
	9800 5700 9900 5700
Wire Wire Line
	10500 5700 10400 5700
NoConn ~ 9900 5900
Wire Wire Line
	10400 5900 10500 5900
Text Notes 8850 5450 0    50   Italic 0
AFAIK SWIM is only used for STM8\nOnly take 5V from SWD, because\nusing on-board 3.3V supply
Wire Wire Line
	9550 5800 9900 5800
$Comp
L power:GND #PWR083
U 1 1 5DDF2535
P 10800 5800
F 0 "#PWR083" H 10800 5550 50  0001 C CNN
F 1 "GND" H 10805 5627 50  0000 C CNN
F 2 "" H 10800 5800 50  0001 C CNN
F 3 "" H 10800 5800 50  0001 C CNN
	1    10800 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10800 5800 10400 5800
Wire Wire Line
	9900 6100 9800 6100
Wire Wire Line
	10400 6100 10500 6100
$Comp
L power:+5V #PWR082
U 1 1 5DE0483D
P 10500 6100
F 0 "#PWR082" H 10500 5950 50  0001 C CNN
F 1 "+5V" H 10515 6273 50  0000 C CNN
F 2 "" H 10500 6100 50  0001 C CNN
F 3 "" H 10500 6100 50  0001 C CNN
	1    10500 6100
	0    1    1    0   
$EndComp
Wire Notes Line
	11100 5200 11100 6450
Wire Notes Line
	11100 6450 8850 6450
Wire Notes Line
	8850 6450 8850 5200
Wire Notes Line
	8850 5200 11100 5200
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J20
U 1 1 5D6E6600
P 10100 5900
F 0 "J20" H 10150 6350 50  0000 C CNN
F 1 "AVR_ISP10_Male" H 10150 6250 50  0000 C CNN
F 2 "Connectors_IDC:IDC-Header_2x05_Pitch2.54mm_Straight" H 10100 5900 50  0001 C CNN
F 3 "~" H 10100 5900 50  0001 C CNN
	1    10100 5900
	1    0    0    -1  
$EndComp
Text GLabel 7250 3900 2    50   Input ~ 0
SWDIO
Wire Wire Line
	7250 3900 7150 3900
Text GLabel 7250 4000 2    50   Input ~ 0
SWCLK
Wire Wire Line
	7150 4000 7250 4000
Text GLabel 7250 2600 2    50   Input ~ 0
ADC_V_BATT
Text GLabel 7250 3300 2    50   Input ~ 0
ADC_I_MAIN
Text GLabel 7250 2900 2    50   Input ~ 0
ADC_I_TAIL1
Text GLabel 7250 3200 2    50   Input ~ 0
ADC_I_TAIL2
Text GLabel 7250 2800 2    50   Input ~ 0
ADC_I_SERVO
Text GLabel 7250 2700 2    50   Input ~ 0
ADC_I_AVIO
Wire Wire Line
	7150 2600 7250 2600
Wire Wire Line
	7250 2700 7150 2700
Wire Wire Line
	7150 2800 7250 2800
Wire Wire Line
	7250 2900 7150 2900
Wire Wire Line
	7150 3200 7250 3200
Wire Wire Line
	7250 3300 7150 3300
NoConn ~ 7150 3000
NoConn ~ 7150 3100
Wire Wire Line
	7250 4900 7150 4900
Wire Wire Line
	7250 5000 7150 5000
Wire Wire Line
	5650 4300 5750 4300
Wire Wire Line
	5750 4400 5650 4400
Wire Wire Line
	5650 4500 5750 4500
Wire Wire Line
	5750 4600 5650 4600
$Comp
L MCU_ST_STM32F1:STM32F103RCTx U8
U 1 1 5DD87643
P 6450 4200
F 0 "U8" H 5450 4950 50  0000 C CNN
F 1 "STM32F103RCTx" H 5450 4850 50  0000 C CNN
F 2 "Housings_QFP:LQFP-64_10x10mm_Pitch0.5mm" H 5850 2500 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00191185.pdf" H 6450 4200 50  0001 C CNN
	1    6450 4200
	1    0    0    -1  
$EndComp
NoConn ~ 7150 3400
NoConn ~ 7150 3700
NoConn ~ 7150 3800
NoConn ~ 7150 4100
NoConn ~ 7150 4300
NoConn ~ 7150 4400
NoConn ~ 7150 4500
NoConn ~ 7150 4600
NoConn ~ 7150 4700
NoConn ~ 7150 4800
NoConn ~ 7150 5100
NoConn ~ 7150 5200
NoConn ~ 7150 5300
NoConn ~ 7150 5400
NoConn ~ 7150 5500
NoConn ~ 7150 5600
NoConn ~ 7150 5700
NoConn ~ 7150 5800
NoConn ~ 5750 5600
NoConn ~ 5750 5500
NoConn ~ 5750 5400
NoConn ~ 5750 5300
NoConn ~ 5750 5200
NoConn ~ 5750 5100
NoConn ~ 5750 5000
NoConn ~ 5750 4900
NoConn ~ 5750 4700
NoConn ~ 5750 4100
NoConn ~ 5750 4000
NoConn ~ 5750 3900
$Comp
L power:GND #PWR071
U 1 1 5DF27E51
P 6450 6200
F 0 "#PWR071" H 6450 5950 50  0001 C CNN
F 1 "GND" H 6455 6027 50  0000 C CNN
F 2 "" H 6450 6200 50  0001 C CNN
F 3 "" H 6450 6200 50  0001 C CNN
	1    6450 6200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 6000 6250 6100
Wire Wire Line
	6250 6100 6350 6100
Wire Wire Line
	6350 6100 6350 6000
Connection ~ 6350 6100
Wire Wire Line
	6350 6100 6450 6100
Wire Wire Line
	6450 6000 6450 6100
Connection ~ 6450 6100
Wire Wire Line
	6550 6000 6550 6100
Wire Wire Line
	6550 6100 6450 6100
Wire Wire Line
	6650 6000 6650 6100
Wire Wire Line
	6650 6100 6550 6100
Connection ~ 6550 6100
Wire Wire Line
	6450 6100 6450 6200
Wire Wire Line
	1550 2850 1550 3000
$Comp
L power:GND #PWR046
U 1 1 5D88F980
P 1550 3000
F 0 "#PWR046" H 1550 2750 50  0001 C CNN
F 1 "GND" H 1555 2827 50  0000 C CNN
F 2 "" H 1550 3000 50  0001 C CNN
F 3 "" H 1550 3000 50  0001 C CNN
	1    1550 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR051
U 1 1 5DF533BE
P 2050 3000
F 0 "#PWR051" H 2050 2750 50  0001 C CNN
F 1 "GND" H 2055 2827 50  0000 C CNN
F 2 "" H 2050 3000 50  0001 C CNN
F 3 "" H 2050 3000 50  0001 C CNN
	1    2050 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR053
U 1 1 5DF5356E
P 2300 3000
F 0 "#PWR053" H 2300 2750 50  0001 C CNN
F 1 "GND" H 2305 2827 50  0000 C CNN
F 2 "" H 2300 3000 50  0001 C CNN
F 3 "" H 2300 3000 50  0001 C CNN
	1    2300 3000
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR055
U 1 1 5DF5371D
P 2850 3400
F 0 "#PWR055" H 2850 3150 50  0001 C CNN
F 1 "GND" H 2855 3227 50  0000 C CNN
F 2 "" H 2850 3400 50  0001 C CNN
F 3 "" H 2850 3400 50  0001 C CNN
	1    2850 3400
	1    0    0    -1  
$EndComp
NoConn ~ 10400 6000
NoConn ~ 9900 6000
$Comp
L Device:C C24
U 1 1 5DF8BB24
P 4950 2050
F 0 "C24" H 5065 2096 50  0000 L CNN
F 1 "100nF" H 5065 2005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4988 1900 50  0001 C CNN
F 3 "~" H 4950 2050 50  0001 C CNN
	1    4950 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C22
U 1 1 5DF8BEAB
P 4500 2050
F 0 "C22" H 4615 2096 50  0000 L CNN
F 1 "100nF" H 4615 2005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4538 1900 50  0001 C CNN
F 3 "~" H 4500 2050 50  0001 C CNN
	1    4500 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C21
U 1 1 5DF8C1CA
P 4050 2050
F 0 "C21" H 4165 2096 50  0000 L CNN
F 1 "100nF" H 4165 2005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 4088 1900 50  0001 C CNN
F 3 "~" H 4050 2050 50  0001 C CNN
	1    4050 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5DF8C695
P 3650 2050
F 0 "C17" H 3765 2096 50  0000 L CNN
F 1 "10uF" H 3765 2005 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 3688 1900 50  0001 C CNN
F 3 "~" H 3650 2050 50  0001 C CNN
	1    3650 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 1900 4050 1900
Wire Wire Line
	6250 1900 6250 2400
Connection ~ 4050 1900
Wire Wire Line
	4050 1900 4500 1900
Connection ~ 4500 1900
Wire Wire Line
	4500 1900 4950 1900
Connection ~ 4950 1900
Wire Wire Line
	4950 1900 5400 1900
Connection ~ 5400 1900
Wire Wire Line
	5400 1900 5850 1900
Connection ~ 5850 1900
Wire Wire Line
	5850 1900 6250 1900
Wire Wire Line
	6250 1900 6350 1900
Wire Wire Line
	6350 1900 6350 2400
Connection ~ 6250 1900
Wire Wire Line
	6650 2400 6650 1900
Wire Wire Line
	6650 1900 6550 1900
Connection ~ 6350 1900
Wire Wire Line
	6450 1900 6450 2400
Wire Wire Line
	6550 2400 6550 1900
Connection ~ 6550 1900
Wire Wire Line
	6550 1900 6450 1900
Connection ~ 6450 1900
Wire Wire Line
	6450 1900 6350 1900
$Comp
L power:GND #PWR068
U 1 1 5DFBC26D
P 5400 2200
F 0 "#PWR068" H 5400 1950 50  0001 C CNN
F 1 "GND" H 5405 2027 50  0000 C CNN
F 2 "" H 5400 2200 50  0001 C CNN
F 3 "" H 5400 2200 50  0001 C CNN
	1    5400 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR066
U 1 1 5DFBC4F1
P 4950 2200
F 0 "#PWR066" H 4950 1950 50  0001 C CNN
F 1 "GND" H 4955 2027 50  0000 C CNN
F 2 "" H 4950 2200 50  0001 C CNN
F 3 "" H 4950 2200 50  0001 C CNN
	1    4950 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR064
U 1 1 5DFBC5F5
P 4500 2200
F 0 "#PWR064" H 4500 1950 50  0001 C CNN
F 1 "GND" H 4505 2027 50  0000 C CNN
F 2 "" H 4500 2200 50  0001 C CNN
F 3 "" H 4500 2200 50  0001 C CNN
	1    4500 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR063
U 1 1 5DFBC829
P 4050 2200
F 0 "#PWR063" H 4050 1950 50  0001 C CNN
F 1 "GND" H 4055 2027 50  0000 C CNN
F 2 "" H 4050 2200 50  0001 C CNN
F 3 "" H 4050 2200 50  0001 C CNN
	1    4050 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR059
U 1 1 5DFBCA0C
P 3650 2200
F 0 "#PWR059" H 3650 1950 50  0001 C CNN
F 1 "GND" H 3655 2027 50  0000 C CNN
F 2 "" H 3650 2200 50  0001 C CNN
F 3 "" H 3650 2200 50  0001 C CNN
	1    3650 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 2400 6750 1900
$Comp
L Device:C C10
U 1 1 5DDAB959
P 1700 4650
F 0 "C10" H 1815 4696 50  0000 L CNN
F 1 "1uF" H 1815 4605 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 1738 4500 50  0001 C CNN
F 3 "~" H 1700 4650 50  0001 C CNN
	1    1700 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R13
U 1 1 5DDAC401
P 1450 4400
F 0 "R13" H 1520 4446 50  0000 L CNN
F 1 "1k" H 1520 4355 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1380 4400 50  0001 C CNN
F 3 "~" H 1450 4400 50  0001 C CNN
	1    1450 4400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR047
U 1 1 5DDACEBB
P 1700 4800
F 0 "#PWR047" H 1700 4550 50  0001 C CNN
F 1 "GND" H 1705 4627 50  0000 C CNN
F 2 "" H 1700 4800 50  0001 C CNN
F 3 "" H 1700 4800 50  0001 C CNN
	1    1700 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 5550 1100 5650
Wire Wire Line
	1100 4300 1100 4400
Wire Wire Line
	1300 4400 1100 4400
Connection ~ 1100 4400
Wire Wire Line
	1100 4400 1100 4500
Wire Wire Line
	1600 4400 1700 4400
Wire Wire Line
	1700 4400 1700 4500
Connection ~ 1700 4400
Wire Wire Line
	1700 4400 1800 4400
$Comp
L Device:C C11
U 1 1 5DDFE67E
P 1700 5900
F 0 "C11" H 1815 5946 50  0000 L CNN
F 1 "1uF" H 1815 5855 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 1738 5750 50  0001 C CNN
F 3 "~" H 1700 5900 50  0001 C CNN
	1    1700 5900
	1    0    0    -1  
$EndComp
$Comp
L Device:R R14
U 1 1 5DDFE688
P 1450 5650
F 0 "R14" H 1520 5696 50  0000 L CNN
F 1 "1k" H 1520 5605 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1380 5650 50  0001 C CNN
F 3 "~" H 1450 5650 50  0001 C CNN
	1    1450 5650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR048
U 1 1 5DDFE692
P 1700 6050
F 0 "#PWR048" H 1700 5800 50  0001 C CNN
F 1 "GND" H 1705 5877 50  0000 C CNN
F 2 "" H 1700 6050 50  0001 C CNN
F 3 "" H 1700 6050 50  0001 C CNN
	1    1700 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5650 1100 5650
Wire Wire Line
	1600 5650 1700 5650
Wire Wire Line
	1700 5650 1700 5750
Connection ~ 1700 5650
Wire Wire Line
	1700 5650 1800 5650
Connection ~ 1100 5650
Wire Wire Line
	1100 5650 1100 5750
$Comp
L Device:C C18
U 1 1 5DE04F92
P 3700 4650
F 0 "C18" H 3815 4696 50  0000 L CNN
F 1 "1uF" H 3815 4605 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 3738 4500 50  0001 C CNN
F 3 "~" H 3700 4650 50  0001 C CNN
	1    3700 4650
	1    0    0    -1  
$EndComp
$Comp
L Device:R R20
U 1 1 5DE04F9C
P 3450 4400
F 0 "R20" H 3520 4446 50  0000 L CNN
F 1 "1k" H 3520 4355 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3380 4400 50  0001 C CNN
F 3 "~" H 3450 4400 50  0001 C CNN
	1    3450 4400
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR060
U 1 1 5DE04FA6
P 3700 4800
F 0 "#PWR060" H 3700 4550 50  0001 C CNN
F 1 "GND" H 3705 4627 50  0000 C CNN
F 2 "" H 3700 4800 50  0001 C CNN
F 3 "" H 3700 4800 50  0001 C CNN
	1    3700 4800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 4400 3700 4400
Wire Wire Line
	3700 4400 3700 4500
Connection ~ 3700 4400
Wire Wire Line
	3700 4400 3800 4400
Wire Notes Line
	550  7600 2400 7600
Wire Notes Line
	2400 7600 2400 3800
Wire Wire Line
	3100 4400 3300 4400
Wire Wire Line
	1100 6800 1100 6900
Text HLabel 3100 5250 0    50   Input ~ 0
VIN_I_SERVO
Text HLabel 3100 6050 0    50   Input ~ 0
VIN_I_AVIO
$Comp
L Device:C C12
U 1 1 5DE73992
P 1700 7150
F 0 "C12" H 1815 7196 50  0000 L CNN
F 1 "1uF" H 1815 7105 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 1738 7000 50  0001 C CNN
F 3 "~" H 1700 7150 50  0001 C CNN
	1    1700 7150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R15
U 1 1 5DE7399C
P 1450 6900
F 0 "R15" H 1520 6946 50  0000 L CNN
F 1 "1k" H 1520 6855 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 1380 6900 50  0001 C CNN
F 3 "~" H 1450 6900 50  0001 C CNN
	1    1450 6900
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR049
U 1 1 5DE739A6
P 1700 7300
F 0 "#PWR049" H 1700 7050 50  0001 C CNN
F 1 "GND" H 1705 7127 50  0000 C CNN
F 2 "" H 1700 7300 50  0001 C CNN
F 3 "" H 1700 7300 50  0001 C CNN
	1    1700 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 6900 1100 6900
Wire Wire Line
	1600 6900 1700 6900
Wire Wire Line
	1700 6900 1700 7000
Connection ~ 1700 6900
Wire Wire Line
	1700 6900 1800 6900
Connection ~ 1100 6900
Wire Wire Line
	1100 6900 1100 7000
$Comp
L Device:C C19
U 1 1 5DE98B9B
P 3700 5500
F 0 "C19" H 3815 5546 50  0000 L CNN
F 1 "1uF" H 3815 5455 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 3738 5350 50  0001 C CNN
F 3 "~" H 3700 5500 50  0001 C CNN
	1    3700 5500
	1    0    0    -1  
$EndComp
$Comp
L Device:R R21
U 1 1 5DE98BA5
P 3450 5250
F 0 "R21" H 3520 5296 50  0000 L CNN
F 1 "1k" H 3520 5205 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3380 5250 50  0001 C CNN
F 3 "~" H 3450 5250 50  0001 C CNN
	1    3450 5250
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR061
U 1 1 5DE98BAF
P 3700 5650
F 0 "#PWR061" H 3700 5400 50  0001 C CNN
F 1 "GND" H 3705 5477 50  0000 C CNN
F 2 "" H 3700 5650 50  0001 C CNN
F 3 "" H 3700 5650 50  0001 C CNN
	1    3700 5650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 5250 3700 5250
Wire Wire Line
	3700 5250 3700 5350
Connection ~ 3700 5250
Wire Wire Line
	3700 5250 3800 5250
Wire Wire Line
	3100 5250 3300 5250
$Comp
L Device:C C20
U 1 1 5DEA1A7F
P 3700 6300
F 0 "C20" H 3815 6346 50  0000 L CNN
F 1 "1uF" H 3815 6255 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 3738 6150 50  0001 C CNN
F 3 "~" H 3700 6300 50  0001 C CNN
	1    3700 6300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R22
U 1 1 5DEA1A89
P 3450 6050
F 0 "R22" H 3520 6096 50  0000 L CNN
F 1 "1k" H 3520 6005 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 3380 6050 50  0001 C CNN
F 3 "~" H 3450 6050 50  0001 C CNN
	1    3450 6050
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR062
U 1 1 5DEA1A93
P 3700 6450
F 0 "#PWR062" H 3700 6200 50  0001 C CNN
F 1 "GND" H 3705 6277 50  0000 C CNN
F 2 "" H 3700 6450 50  0001 C CNN
F 3 "" H 3700 6450 50  0001 C CNN
	1    3700 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 6050 3700 6050
Wire Wire Line
	3700 6050 3700 6150
Connection ~ 3700 6050
Wire Wire Line
	3700 6050 3800 6050
Wire Wire Line
	3100 6050 3300 6050
Wire Notes Line
	2500 5150 4350 5150
Wire Notes Line
	4350 5150 4350 6700
Wire Notes Line
	4350 6700 2500 6700
Wire Notes Line
	2500 6700 2500 5150
Text Notes 2500 6700 0    50   ~ 0
Other ADC filters
Text HLabel 5650 4300 0    50   BiDi ~ 0
GPIO_0
Text HLabel 5650 4400 0    50   BiDi ~ 0
GPIO_1
Text HLabel 5650 4500 0    50   BiDi ~ 0
GPIO_2
Text HLabel 5650 4600 0    50   BiDi ~ 0
GPIO_3
Connection ~ 6750 1900
$Comp
L power:GND #PWR074
U 1 1 5DF54159
P 7450 2200
F 0 "#PWR074" H 7450 1950 50  0001 C CNN
F 1 "GND" H 7455 2027 50  0000 C CNN
F 2 "" H 7450 2200 50  0001 C CNN
F 3 "" H 7450 2200 50  0001 C CNN
	1    7450 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR073
U 1 1 5DF5399C
P 6950 2200
F 0 "#PWR073" H 6950 1950 50  0001 C CNN
F 1 "GND" H 6955 2027 50  0000 C CNN
F 2 "" H 6950 2200 50  0001 C CNN
F 3 "" H 6950 2200 50  0001 C CNN
	1    6950 2200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 1900 6950 1900
Wire Wire Line
	6950 1900 7450 1900
Connection ~ 6950 1900
$Comp
L Device:C C27
U 1 1 5D615A1F
P 6950 2050
F 0 "C27" H 7065 2096 50  0000 L CNN
F 1 "100nF" H 7065 2005 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 6988 1900 50  0001 C CNN
F 3 "~" H 6950 2050 50  0001 C CNN
	1    6950 2050
	1    0    0    -1  
$EndComp
$Comp
L Device:C C28
U 1 1 5D667930
P 7450 2050
F 0 "C28" H 7565 2096 50  0000 L CNN
F 1 "1uF" H 7565 2005 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-A_EIA-3216-18_Hand" H 7488 1900 50  0001 C CNN
F 3 "~" H 7450 2050 50  0001 C CNN
	1    7450 2050
	1    0    0    -1  
$EndComp
Text Label 1100 4400 2    50   ~ 0
DIV_I_MAIN
Text Label 1100 5650 2    50   ~ 0
DIV_I_TAIL1
Text Label 1100 6900 2    50   ~ 0
DIV_I_TAIL2
Text Label 5300 2800 0    50   ~ 0
BOOT0
Text Label 3100 4400 2    50   ~ 0
DIV_V_BATT
$EndSCHEMATC
