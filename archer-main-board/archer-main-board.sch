EESchema Schematic File Version 4
LIBS:archer-main-board-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "ARCHER Mainboard"
Date "28.11.2019"
Rev "1"
Comp "Warsaw University of Technology"
Comment1 "Marek Łukasiewicz"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	10100 2750 10200 2750
$Comp
L power:+5V #PWR0101
U 1 1 5DCCA72F
P 9400 2750
F 0 "#PWR0101" H 9400 2600 50  0001 C CNN
F 1 "+5V" H 9415 2923 50  0000 C CNN
F 2 "" H 9400 2750 50  0001 C CNN
F 3 "" H 9400 2750 50  0001 C CNN
	1    9400 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9400 2750 9500 2750
Wire Notes Line
	9250 2400 11000 2400
$Comp
L Connector_Generic:Conn_01x08 J1
U 1 1 5DCEACD1
P 1350 1050
F 0 "J1" H 1430 1042 50  0000 L CNN
F 1 "ClickMate_1m5" H 1430 951 50  0000 L CNN
F 2 "Connector_Molex:Molex_CLIK-Mate_505405-0870_1x08-1MP_P1.50mm_Vertical" H 1350 1050 50  0001 C CNN
F 3 "~" H 1350 1050 50  0001 C CNN
	1    1350 1050
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x08 J2
U 1 1 5DD05307
P 1350 2050
F 0 "J2" H 1430 2042 50  0000 L CNN
F 1 "ClickMate_1m5" H 1430 1951 50  0000 L CNN
F 2 "Connector_Molex:Molex_CLIK-Mate_505405-0870_1x08-1MP_P1.50mm_Vertical" H 1350 2050 50  0001 C CNN
F 3 "~" H 1350 2050 50  0001 C CNN
	1    1350 2050
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J4
U 1 1 5DD0584D
P 1950 2450
F 0 "J4" H 2030 2442 50  0000 L CNN
F 1 "Pad" H 2030 2351 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 1950 2450 50  0001 C CNN
F 3 "~" H 1950 2450 50  0001 C CNN
	1    1950 2450
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J5
U 1 1 5DD090DE
P 1950 2650
F 0 "J5" H 2030 2642 50  0000 L CNN
F 1 "Pad" H 2030 2551 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 1950 2650 50  0001 C CNN
F 3 "~" H 1950 2650 50  0001 C CNN
	1    1950 2650
	1    0    0    -1  
$EndComp
Text Notes 2200 2650 0    50   ~ 0
Future\npix conn
Text GLabel 1750 2250 2    50   Input ~ 0
PIX_PWM_AUX6
Text GLabel 1750 2150 2    50   Input ~ 0
PIX_PWM_AUX5
Text GLabel 1750 2050 2    50   Input ~ 0
PIX_PWM_AUX4
Text GLabel 1750 1950 2    50   Input ~ 0
PIX_PWM_AUX3
Text GLabel 1750 1850 2    50   Input ~ 0
PIX_PWM_AUX2
Text GLabel 1750 1750 2    50   Input ~ 0
PIX_PWM_AUX1
Wire Wire Line
	1550 1750 1750 1750
Wire Wire Line
	1750 1850 1550 1850
Wire Wire Line
	1550 1950 1750 1950
Wire Wire Line
	1750 2050 1550 2050
Wire Wire Line
	1550 2150 1750 2150
Wire Wire Line
	1750 2250 1550 2250
Text GLabel 1750 750  2    50   Input ~ 0
PIX_PWM_MAIN1
Text GLabel 1750 850  2    50   Input ~ 0
PIX_PWM_MAIN2
Text GLabel 1750 950  2    50   Input ~ 0
PIX_PWM_MAIN3
Text GLabel 1750 1050 2    50   Input ~ 0
PIX_PWM_MAIN4
Text GLabel 1750 1150 2    50   Input ~ 0
PIX_PWM_MAIN5
Text GLabel 1750 1250 2    50   Input ~ 0
PIX_PWM_MAIN6
Text GLabel 1750 1350 2    50   Input ~ 0
PIX_PWM_MAIN7
Text GLabel 1750 1450 2    50   Input ~ 0
PIX_PWM_MAIN8
Wire Wire Line
	1550 750  1750 750 
Wire Wire Line
	1750 850  1550 850 
Wire Wire Line
	1550 950  1750 950 
Wire Wire Line
	1750 1050 1550 1050
Wire Wire Line
	1550 1150 1750 1150
Wire Wire Line
	1750 1250 1550 1250
Wire Wire Line
	1550 1350 1750 1350
Wire Wire Line
	1750 1450 1550 1450
Wire Wire Line
	1550 2350 1750 2350
Wire Wire Line
	1750 2350 1750 2450
Wire Wire Line
	1550 2450 1650 2450
Wire Wire Line
	1650 2450 1650 2650
Wire Wire Line
	1650 2650 1750 2650
Wire Notes Line
	3600 2850 3600 650 
Wire Notes Line
	3600 650  650  650 
Wire Notes Line
	650  650  650  2850
Wire Notes Line
	650  2850 3600 2850
Text Notes 650  2850 0    50   ~ 0
Pixhawk PWM Connection
Text Notes 700  950  0    50   ~ 0
Main channels
Text Notes 700  1950 0    50   ~ 0
Aux channels
Wire Wire Line
	8950 4150 8950 4800
Connection ~ 3550 4700
Wire Wire Line
	4000 4400 5550 4400
Wire Wire Line
	4000 4700 4000 4400
Wire Wire Line
	3550 4700 4000 4700
Wire Wire Line
	3250 6050 3350 6050
$Sheet
S 5550 3800 1600 1600
U 5D5FEF01
F0 "Sheet Main STM" 50
F1 "fileMainSTM.sch" 50
F2 "SPI_CS" O L 5550 3900 50 
F3 "SPI_SCLK" O L 5550 4000 50 
F4 "SPI_MISO" I L 5550 4200 50 
F5 "SPI_MOSI" O L 5550 4100 50 
F6 "1WIRE_DQ" I L 5550 4400 50 
F7 "I2C_SCK" B L 5550 4600 50 
F8 "I2C_SDA" B L 5550 4700 50 
F9 "RPM_TAIL2" I R 7150 3950 50 
F10 "RPM_TAIL1" I R 7150 4050 50 
F11 "RPM_MAIN" I R 7150 4150 50 
F12 "UART_OUT_RX" I R 7150 4500 50 
F13 "UART_OUT_TX" O R 7150 4400 50 
F14 "UART_PWR_RX" I R 7150 4800 50 
F15 "UART_PWR_TX" O R 7150 4700 50 
F16 "DAC_BAT_CURR" O R 7150 5100 50 
F17 "DAC_BATT_VOLT" O R 7150 5000 50 
F18 "GPIO_0" B L 5550 4900 50 
F19 "GPIO_1" B L 5550 5000 50 
F20 "GPIO_2" B L 5550 5100 50 
F21 "GPIO_3" B L 5550 5200 50 
$EndSheet
Wire Wire Line
	8550 4400 7150 4400
Wire Wire Line
	7150 4500 8650 4500
Wire Wire Line
	7150 4700 8850 4700
Wire Wire Line
	7150 4800 8950 4800
Wire Wire Line
	7150 5100 8850 5100
Wire Wire Line
	7150 5000 8950 5000
Wire Wire Line
	8650 4500 8650 2950
Wire Wire Line
	8550 2850 8550 4400
Wire Wire Line
	10200 2850 8550 2850
Wire Wire Line
	8650 2950 10200 2950
Wire Notes Line
	9250 6450 11000 6450
Wire Notes Line
	9250 5350 11000 5350
Wire Notes Line
	9250 5350 9250 6450
Text Notes 9250 6450 0    50   ~ 0
Pixhawk power Connection (like power brick)
Wire Notes Line
	11000 6450 11000 5350
Wire Wire Line
	8850 5950 10350 5950
Wire Wire Line
	8850 5100 8850 5950
Wire Wire Line
	8950 5850 10350 5850
Wire Wire Line
	8950 5000 8950 5850
Connection ~ 10250 6150
Wire Wire Line
	10350 6150 10250 6150
Wire Wire Line
	10250 6050 10350 6050
Wire Wire Line
	10250 6150 10250 6050
$Comp
L power:GND #PWR0102
U 1 1 5DCBF94F
P 10250 6150
F 0 "#PWR0102" H 10250 5900 50  0001 C CNN
F 1 "GND" H 10255 5977 50  0000 C CNN
F 2 "" H 10250 6150 50  0001 C CNN
F 3 "" H 10250 6150 50  0001 C CNN
	1    10250 6150
	-1   0    0    -1  
$EndComp
Connection ~ 10250 5650
Wire Wire Line
	10250 5750 10250 5650
Wire Wire Line
	10350 5750 10250 5750
Wire Wire Line
	10350 5650 10250 5650
$Comp
L power:+5V #PWR0103
U 1 1 5DCBBF27
P 10250 5650
F 0 "#PWR0103" H 10250 5500 50  0001 C CNN
F 1 "+5V" H 10265 5823 50  0000 C CNN
F 2 "" H 10250 5650 50  0001 C CNN
F 3 "" H 10250 5650 50  0001 C CNN
	1    10250 5650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J19
U 1 1 5DCB9B91
P 10550 5850
F 0 "J19" H 10630 5842 50  0000 L CNN
F 1 "JST-GH" H 10630 5751 50  0000 L CNN
F 2 "Connectors_JST:JST_GH_BM06B-GHS-TBT_06x1.25mm_Straight" H 10550 5850 50  0001 C CNN
F 3 "~" H 10550 5850 50  0001 C CNN
	1    10550 5850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 4150 8950 4150
Wire Wire Line
	8850 4050 10200 4050
Wire Wire Line
	8850 4700 8850 4050
Text Notes 9250 3600 0    50   ~ 0
Mission computer Connection
Wire Notes Line
	11000 3600 9250 3600
Wire Notes Line
	11000 2400 11000 3600
Wire Notes Line
	9250 3600 9250 2400
Wire Notes Line
	11000 5250 11000 3700
Wire Notes Line
	9250 3700 9250 5250
Wire Wire Line
	10200 3300 10100 3300
Wire Wire Line
	10200 3050 10200 3300
Wire Wire Line
	9500 3300 9400 3300
$Comp
L power:GND #PWR0104
U 1 1 5DCCA744
P 9400 3300
F 0 "#PWR0104" H 9400 3050 50  0001 C CNN
F 1 "GND" H 9405 3127 50  0000 C CNN
F 2 "" H 9400 3300 50  0001 C CNN
F 3 "" H 9400 3300 50  0001 C CNN
	1    9400 3300
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper JP2
U 1 1 5DCCA73A
P 9800 3300
F 0 "JP2" H 9800 3564 50  0000 C CNN
F 1 "Jumper" H 9800 3473 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9800 3300 50  0001 C CNN
F 3 "~" H 9800 3300 50  0001 C CNN
	1    9800 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:Jumper JP1
U 1 1 5DCCA724
P 9800 2750
F 0 "JP1" H 9800 3014 50  0000 C CNN
F 1 "Jumper" H 9800 2923 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9800 2750 50  0001 C CNN
F 3 "~" H 9800 2750 50  0001 C CNN
	1    9800 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J15
U 1 1 5DCCA71A
P 10400 2850
F 0 "J15" H 10480 2842 50  0000 L CNN
F 1 "JST-XH_Male" H 10480 2751 50  0000 L CNN
F 2 "Connectors_JST:JST_XH_S04B-XH-A_04x2.50mm_Angled" H 10400 2850 50  0001 C CNN
F 3 "~" H 10400 2850 50  0001 C CNN
	1    10400 2850
	1    0    0    -1  
$EndComp
Text Notes 650  7700 0    50   ~ 0
SPI Connection
Wire Notes Line
	650  7700 650  6600
Wire Notes Line
	1850 7700 650  7700
Wire Notes Line
	1850 6600 1850 7700
Wire Notes Line
	650  6600 1850 6600
Wire Wire Line
	1600 7400 1700 7400
$Comp
L power:GND #PWR0105
U 1 1 5DCBF32C
P 1700 7400
F 0 "#PWR0105" H 1700 7150 50  0001 C CNN
F 1 "GND" H 1705 7227 50  0000 C CNN
F 2 "" H 1700 7400 50  0001 C CNN
F 3 "" H 1700 7400 50  0001 C CNN
	1    1700 7400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1600 6900 1700 6900
$Comp
L power:+5V #PWR0106
U 1 1 5DCBDEBE
P 1700 6900
F 0 "#PWR0106" H 1700 6750 50  0001 C CNN
F 1 "+5V" H 1715 7073 50  0000 C CNN
F 2 "" H 1700 6900 50  0001 C CNN
F 3 "" H 1700 6900 50  0001 C CNN
	1    1700 6900
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J3
U 1 1 5DCBD918
P 1400 7100
F 0 "J3" H 1480 7092 50  0000 L CNN
F 1 "Goldpin_Male" H 1480 7001 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 1400 7100 50  0001 C CNN
F 3 "~" H 1400 7100 50  0001 C CNN
	1    1400 7100
	-1   0    0    -1  
$EndComp
Text Notes 3100 5350 2    50   ~ 0
1-Wire Hub
Wire Notes Line
	2650 5350 2650 4400
Wire Notes Line
	3650 5350 2650 5350
Wire Notes Line
	3650 4400 3650 5350
Wire Notes Line
	2650 4400 3650 4400
Wire Wire Line
	2850 5000 2850 4900
Connection ~ 2850 5000
Wire Wire Line
	2950 5000 2850 5000
Wire Wire Line
	2850 4900 2850 4800
Connection ~ 2850 4900
Wire Wire Line
	2950 4900 2850 4900
Wire Wire Line
	2850 4800 2850 4700
Connection ~ 2850 4800
Wire Wire Line
	2950 4800 2850 4800
Wire Wire Line
	2850 4700 2950 4700
$Comp
L power:GND #PWR0107
U 1 1 5DCB91A5
P 2850 5000
F 0 "#PWR0107" H 2850 4750 50  0001 C CNN
F 1 "GND" H 2855 4827 50  0000 C CNN
F 2 "" H 2850 5000 50  0001 C CNN
F 3 "" H 2850 5000 50  0001 C CNN
	1    2850 5000
	1    0    0    -1  
$EndComp
Connection ~ 3550 4900
Wire Wire Line
	3550 5000 3550 4900
Wire Wire Line
	3450 5000 3550 5000
Connection ~ 3550 4800
Wire Wire Line
	3550 4900 3550 4800
Wire Wire Line
	3450 4900 3550 4900
Wire Wire Line
	3550 4700 3450 4700
Wire Wire Line
	3550 4800 3550 4700
Wire Wire Line
	3450 4800 3550 4800
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J6
U 1 1 5DCB5553
P 3250 4800
F 0 "J6" H 3300 4375 50  0000 C CNN
F 1 "Goldpin_Male" H 3300 4466 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 3250 4800 50  0001 C CNN
F 3 "~" H 3250 4800 50  0001 C CNN
	1    3250 4800
	-1   0    0    -1  
$EndComp
Wire Notes Line
	11000 5250 9250 5250
Wire Wire Line
	10000 4800 10100 4800
Text GLabel 10000 4800 0    50   Input ~ 0
5V_SERVO
Wire Wire Line
	9750 4900 10100 4900
$Comp
L power:GND #PWR0108
U 1 1 5DCB1C30
P 9750 4900
F 0 "#PWR0108" H 9750 4650 50  0001 C CNN
F 1 "GND" H 9755 4727 50  0000 C CNN
F 2 "" H 9750 4900 50  0001 C CNN
F 3 "" H 9750 4900 50  0001 C CNN
	1    9750 4900
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J17
U 1 1 5DCB155B
P 10400 4800
F 0 "J17" H 10480 4792 50  0000 L CNN
F 1 "XT30_Male" H 10480 4701 50  0000 L CNN
F 2 "Connector_AMASS:AMASS_XT30PW-M_1x02_P2.50mm_Horizontal" H 10400 4800 50  0001 C CNN
F 3 "~" H 10400 4800 50  0001 C CNN
	1    10400 4800
	1    0    0    -1  
$EndComp
Wire Notes Line
	9250 3700 11000 3700
Text Notes 9250 5250 0    50   ~ 0
Power board Connection
Wire Wire Line
	9600 4500 9500 4500
$Comp
L power:GND #PWR0109
U 1 1 5DCA5FF9
P 9500 4500
F 0 "#PWR0109" H 9500 4250 50  0001 C CNN
F 1 "GND" H 9505 4327 50  0000 C CNN
F 2 "" H 9500 4500 50  0001 C CNN
F 3 "" H 9500 4500 50  0001 C CNN
	1    9500 4500
	-1   0    0    -1  
$EndComp
$Comp
L Device:Jumper JP3
U 1 1 5DCA5FF3
P 9900 4500
F 0 "JP3" H 9900 4764 50  0000 C CNN
F 1 "Jumper" H 9900 4673 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 9900 4500 50  0001 C CNN
F 3 "~" H 9900 4500 50  0001 C CNN
	1    9900 4500
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0110
U 1 1 5DCA5FEC
P 9700 3950
F 0 "#PWR0110" H 9700 3800 50  0001 C CNN
F 1 "+5V" H 9715 4123 50  0000 C CNN
F 2 "" H 9700 3950 50  0001 C CNN
F 3 "" H 9700 3950 50  0001 C CNN
	1    9700 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 3950 10100 3950
$Comp
L Connector_Generic:Conn_01x04 J16
U 1 1 5DCA5FDF
P 10400 4050
F 0 "J16" H 10480 4042 50  0000 L CNN
F 1 "JST-XH_Male" H 10480 3951 50  0000 L CNN
F 2 "Connectors_JST:JST_XH_S04B-XH-A_04x2.50mm_Angled" H 10400 4050 50  0001 C CNN
F 3 "~" H 10400 4050 50  0001 C CNN
	1    10400 4050
	1    0    0    -1  
$EndComp
Wire Notes Line
	2650 5450 2650 6400
Wire Notes Line
	3650 5450 2650 5450
Wire Notes Line
	3650 6400 3650 5450
Wire Notes Line
	2650 6400 3650 6400
Text Notes 2650 6400 0    50   ~ 0
I2C Connection
$Comp
L power:GND #PWR0111
U 1 1 5D7F8D5B
P 3350 6050
F 0 "#PWR0111" H 3350 5800 50  0001 C CNN
F 1 "GND" H 3355 5877 50  0000 C CNN
F 2 "" H 3350 6050 50  0001 C CNN
F 3 "" H 3350 6050 50  0001 C CNN
	1    3350 6050
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0112
U 1 1 5D7F643A
P 3350 5750
F 0 "#PWR0112" H 3350 5600 50  0001 C CNN
F 1 "+5V" H 3365 5923 50  0000 C CNN
F 2 "" H 3350 5750 50  0001 C CNN
F 3 "" H 3350 5750 50  0001 C CNN
	1    3350 5750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3350 5750 3250 5750
$Comp
L Connector_Generic:Conn_01x04 J7
U 1 1 5D7F5DC9
P 3050 5850
F 0 "J7" H 3130 5842 50  0000 L CNN
F 1 "JST-GH" H 3130 5751 50  0000 L CNN
F 2 "Connectors_JST:JST_GH_BM04B-GHS-TBT_04x1.25mm_Straight" H 3050 5850 50  0001 C CNN
F 3 "~" H 3050 5850 50  0001 C CNN
	1    3050 5850
	-1   0    0    -1  
$EndComp
Text Notes 2450 800  0    50   Italic 0
Swashplate front left servo
Text Notes 2450 900  0    50   Italic 0
Swashplate front right servo
Text Notes 2450 1000 0    50   Italic 0
Swashplate back servo
Text Notes 2450 1500 0    50   Italic 0
Main rotor ESC
Text Notes 2450 1400 0    50   Italic 0
Tail 1 ESC
Text Notes 2450 1300 0    50   ~ 0
Tail 2 ESC
Text Notes 2450 1100 0    50   Italic 0
Tail 1 pitch servo
Text Notes 2450 1200 0    50   ~ 0
Tail 2 pitch servo
Text Notes 2450 1800 0    50   ~ 0
Wing incidence angle servo
Text Notes 2450 1900 0    50   ~ 0
Elevator servo
Text Notes 2450 2000 0    50   ~ 0
Front AUX (gimbal pitch)
Text Notes 2450 2200 0    50   ~ 0
Front AUX (gimbal roll)
Text Notes 2450 2100 0    50   ~ 0
Back AUX (rudder)
Text Notes 2450 2300 0    50   ~ 0
Back AUX
Text Notes 1300 1600 0    50   Italic 0
Functions in italic are on ArduCopter's default channels
Wire Wire Line
	1600 7000 2050 7000
Wire Wire Line
	2050 7000 2050 3900
Wire Wire Line
	2050 3900 5550 3900
Wire Wire Line
	2150 4000 2150 7100
Wire Wire Line
	2150 7100 1600 7100
Wire Wire Line
	2150 4000 5550 4000
Wire Wire Line
	1600 7200 2250 7200
Wire Wire Line
	2250 7200 2250 4100
Wire Wire Line
	2250 4100 5550 4100
Wire Wire Line
	2350 4200 2350 7300
Wire Wire Line
	2350 7300 1600 7300
Wire Wire Line
	2350 4200 5550 4200
Wire Wire Line
	10200 4250 10200 4500
Text Notes 11100 4800 1    50   ~ 0
POS: front right
Text Notes 11100 3300 1    50   ~ 0
POS: back right
Text Notes 11100 6200 1    50   ~ 0
POS: back left
Text Notes 600  2000 1    50   ~ 0
POS: back left
Text Notes 2600 6200 1    50   ~ 0
POS: back left
Text Notes 2600 5200 1    50   ~ 0
POS: insignificant
Text Notes 600  7550 1    50   ~ 0
POS: insignificant
$Comp
L Connector_Generic:Conn_01x05 J14
U 1 1 5DCCBFDA
P 9550 950
F 0 "J14" H 9630 942 50  0000 L CNN
F 1 "Goldpin_Male" H 9630 851 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 9550 950 50  0001 C CNN
F 3 "~" H 9550 950 50  0001 C CNN
	1    9550 950 
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J18
U 1 1 5DCCCDF2
P 10450 850
F 0 "J18" H 10500 425 50  0000 C CNN
F 1 "Goldpin_Male" H 10500 516 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 10450 850 50  0001 C CNN
F 3 "~" H 10450 850 50  0001 C CNN
	1    10450 850 
	1    0    0    -1  
$EndComp
Text GLabel 10150 750  0    50   Input ~ 0
5V_SERVO
Wire Wire Line
	10150 750  10150 850 
Wire Wire Line
	10150 750  10250 750 
Wire Wire Line
	10250 850  10150 850 
Connection ~ 10150 850 
Wire Wire Line
	10150 850  10150 950 
Wire Wire Line
	10150 950  10250 950 
Wire Wire Line
	10750 750  10850 750 
Wire Wire Line
	10850 750  10850 850 
Wire Wire Line
	10850 1050 10750 1050
Wire Wire Line
	10750 950  10850 950 
Connection ~ 10850 950 
Wire Wire Line
	10850 950  10850 1050
Wire Wire Line
	10750 850  10850 850 
Connection ~ 10850 850 
Wire Wire Line
	10850 850  10850 950 
$Comp
L power:GND #PWR0113
U 1 1 5DCF10BD
P 10850 1050
F 0 "#PWR0113" H 10850 800 50  0001 C CNN
F 1 "GND" H 10855 877 50  0000 C CNN
F 2 "" H 10850 1050 50  0001 C CNN
F 3 "" H 10850 1050 50  0001 C CNN
	1    10850 1050
	-1   0    0    -1  
$EndComp
Connection ~ 10850 1050
Text GLabel 9250 750  0    50   Input ~ 0
PIX_PWM_MAIN1
Text GLabel 9250 850  0    50   Input ~ 0
PIX_PWM_MAIN2
Text GLabel 9250 950  0    50   Input ~ 0
PIX_PWM_MAIN3
Wire Wire Line
	9350 750  9250 750 
Wire Wire Line
	9250 850  9350 850 
Wire Wire Line
	9350 950  9250 950 
Text GLabel 9250 1050 0    50   Input ~ 0
PIX_PWM_MAIN8
Wire Wire Line
	9250 1050 9350 1050
Text GLabel 9250 1150 0    50   Input ~ 0
ESC_RPM_MAIN
Wire Wire Line
	9250 1150 9350 1150
NoConn ~ 10250 1050
Text Notes 9700 1400 0    50   Italic 0
Do not connect\nBEC wire on ESC
Text GLabel 7350 4150 2    50   Input ~ 0
ESC_RPM_MAIN
Wire Wire Line
	7150 4150 7350 4150
Wire Notes Line
	8500 600  11000 600 
Wire Notes Line
	11000 600  11000 1500
Wire Notes Line
	11000 1500 8500 1500
Wire Notes Line
	8500 1500 8500 600 
Text Notes 8500 1500 0    50   ~ 0
Classical heli Connection
Text Notes 11100 1400 1    50   ~ 0
POS: front center
$Comp
L Connector_Generic:Conn_01x04 J8
U 1 1 5DD50748
P 6700 950
F 0 "J8" H 6780 942 50  0000 L CNN
F 1 "Goldpin_Male" H 6780 851 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 6700 950 50  0001 C CNN
F 3 "~" H 6700 950 50  0001 C CNN
	1    6700 950 
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J11
U 1 1 5DD50752
P 7600 950
F 0 "J11" H 7650 525 50  0000 C CNN
F 1 "Goldpin_Male" H 7650 616 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 7600 950 50  0001 C CNN
F 3 "~" H 7600 950 50  0001 C CNN
	1    7600 950 
	1    0    0    -1  
$EndComp
Text GLabel 7300 850  0    50   Input ~ 0
5V_SERVO
Wire Wire Line
	7300 850  7300 950 
Wire Wire Line
	7300 850  7400 850 
Wire Wire Line
	7400 950  7300 950 
Connection ~ 7300 950 
Wire Wire Line
	7300 950  7300 1050
Wire Wire Line
	7300 1050 7400 1050
Wire Wire Line
	7900 850  8000 850 
Wire Wire Line
	8000 850  8000 950 
Wire Wire Line
	8000 1150 7900 1150
Wire Wire Line
	7900 1050 8000 1050
Connection ~ 8000 1050
Wire Wire Line
	8000 1050 8000 1150
Wire Wire Line
	7900 950  8000 950 
Connection ~ 8000 950 
Wire Wire Line
	8000 950  8000 1050
$Comp
L power:GND #PWR0114
U 1 1 5DD5076C
P 8000 1150
F 0 "#PWR0114" H 8000 900 50  0001 C CNN
F 1 "GND" H 8005 977 50  0000 C CNN
F 2 "" H 8000 1150 50  0001 C CNN
F 3 "" H 8000 1150 50  0001 C CNN
	1    8000 1150
	-1   0    0    -1  
$EndComp
Connection ~ 8000 1150
Wire Wire Line
	6500 850  6400 850 
Wire Wire Line
	6400 950  6500 950 
Wire Wire Line
	6500 1050 6400 1050
Wire Wire Line
	6400 1150 6500 1150
Wire Notes Line
	5650 600  8150 600 
Wire Notes Line
	8150 600  8150 1500
Wire Notes Line
	8150 1500 5650 1500
Wire Notes Line
	5650 1500 5650 600 
Text Notes 5650 1500 0    50   ~ 0
Front aux Connection
Text Notes 8250 1350 1    50   ~ 0
POS: front left
Text GLabel 6400 850  0    50   Input ~ 0
PIX_PWM_AUX1
Text GLabel 6400 1050 0    50   Input ~ 0
PIX_PWM_AUX3
Text GLabel 6400 1150 0    50   Input ~ 0
PIX_PWM_AUX5
Text GLabel 6400 950  0    50   Input ~ 0
PIX_PWM_AUX1
Wire Wire Line
	7300 1050 7300 1150
Wire Wire Line
	7300 1150 7400 1150
Connection ~ 7300 1050
$Comp
L Connector_Generic:Conn_01x06 J9
U 1 1 5DD77951
P 6700 2000
F 0 "J9" H 6780 1992 50  0000 L CNN
F 1 "Goldpin_Male" H 6780 1901 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x06_Pitch2.54mm" H 6700 2000 50  0001 C CNN
F 3 "~" H 6700 2000 50  0001 C CNN
	1    6700 2000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Odd_Even J12
U 1 1 5DD7795B
P 7600 2000
F 0 "J12" H 7650 1575 50  0000 C CNN
F 1 "Goldpin_Male" H 7650 1666 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x04_Pitch2.54mm" H 7600 2000 50  0001 C CNN
F 3 "~" H 7600 2000 50  0001 C CNN
	1    7600 2000
	1    0    0    -1  
$EndComp
Text GLabel 7300 1900 0    50   Input ~ 0
5V_SERVO
Wire Wire Line
	7300 1900 7300 2000
Wire Wire Line
	7300 2100 7400 2100
Wire Wire Line
	7900 1900 8000 1900
Wire Wire Line
	8000 1900 8000 2000
Wire Wire Line
	8000 2200 7900 2200
Wire Wire Line
	7900 2100 8000 2100
Connection ~ 8000 2100
Wire Wire Line
	8000 2100 8000 2200
Wire Wire Line
	7900 2000 8000 2000
Connection ~ 8000 2000
Wire Wire Line
	8000 2000 8000 2100
$Comp
L power:GND #PWR0115
U 1 1 5DD77975
P 8000 2200
F 0 "#PWR0115" H 8000 1950 50  0001 C CNN
F 1 "GND" H 8005 2027 50  0000 C CNN
F 2 "" H 8000 2200 50  0001 C CNN
F 3 "" H 8000 2200 50  0001 C CNN
	1    8000 2200
	-1   0    0    -1  
$EndComp
Connection ~ 8000 2200
Wire Wire Line
	6500 1800 6400 1800
Wire Wire Line
	6400 1900 6500 1900
Wire Wire Line
	6500 2000 6400 2000
Wire Wire Line
	6400 2100 6500 2100
Wire Wire Line
	6400 2200 6500 2200
NoConn ~ 7400 2200
Text Notes 6800 2450 0    50   Italic 0
Do not connect\nBEC wire on ESC
Wire Notes Line
	5650 1650 8150 1650
Wire Notes Line
	8150 1650 8150 2550
Wire Notes Line
	8150 2550 5650 2550
Wire Notes Line
	5650 2550 5650 1650
Text Notes 5650 2550 0    50   ~ 0
Tail Connection
Text Notes 8250 2450 1    50   ~ 0
POS: (back?) center
NoConn ~ 7400 1900
Text GLabel 6400 2300 0    50   Input ~ 0
ESC_RPM_TAIL2
Text GLabel 6400 1800 0    50   Input ~ 0
ESC_RPM_TAIL1
Wire Wire Line
	6400 2300 6500 2300
Text GLabel 6400 1900 0    50   Input ~ 0
PIX_PWM_MAIN7
Text GLabel 6400 2200 0    50   Input ~ 0
PIX_PWM_MAIN6
Text GLabel 6400 2100 0    50   Input ~ 0
PIX_PWM_MAIN5
Text GLabel 6400 2000 0    50   Input ~ 0
PIX_PWM_MAIN4
$Comp
L Connector_Generic:Conn_01x03 J10
U 1 1 5DD8ECA2
P 6700 3050
F 0 "J10" H 6780 3042 50  0000 L CNN
F 1 "Goldpin_Male" H 6780 2951 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 6700 3050 50  0001 C CNN
F 3 "~" H 6700 3050 50  0001 C CNN
	1    6700 3050
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J13
U 1 1 5DD8ECAC
P 7600 3050
F 0 "J13" H 7650 2625 50  0000 C CNN
F 1 "Goldpin_Male" H 7650 2716 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03_Pitch2.54mm" H 7600 3050 50  0001 C CNN
F 3 "~" H 7600 3050 50  0001 C CNN
	1    7600 3050
	1    0    0    -1  
$EndComp
Text GLabel 7300 2950 0    50   Input ~ 0
5V_SERVO
Wire Wire Line
	7300 2950 7300 3050
Wire Wire Line
	7300 2950 7400 2950
Wire Wire Line
	7400 3050 7300 3050
Connection ~ 7300 3050
Wire Wire Line
	7300 3050 7300 3150
Wire Wire Line
	7300 3150 7400 3150
Wire Wire Line
	7900 2950 8000 2950
Wire Wire Line
	8000 2950 8000 3050
Wire Wire Line
	7900 3150 8000 3150
Connection ~ 8000 3150
Wire Wire Line
	8000 3150 8000 3250
Wire Wire Line
	7900 3050 8000 3050
Connection ~ 8000 3050
Wire Wire Line
	8000 3050 8000 3150
$Comp
L power:GND #PWR0116
U 1 1 5DD8ECC6
P 8000 3250
F 0 "#PWR0116" H 8000 3000 50  0001 C CNN
F 1 "GND" H 8005 3077 50  0000 C CNN
F 2 "" H 8000 3250 50  0001 C CNN
F 3 "" H 8000 3250 50  0001 C CNN
	1    8000 3250
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6500 2950 6400 2950
Wire Wire Line
	6400 3050 6500 3050
Wire Wire Line
	6500 3150 6400 3150
Wire Notes Line
	5650 2700 8150 2700
Wire Notes Line
	8150 2700 8150 3600
Wire Notes Line
	8150 3600 5650 3600
Wire Notes Line
	5650 3600 5650 2700
Text Notes 5650 3600 0    50   ~ 0
Back aux Connection
Text Notes 8250 3450 1    50   ~ 0
POS: back right
Text GLabel 6400 2950 0    50   Input ~ 0
PIX_PWM_AUX2
Text GLabel 6400 3050 0    50   Input ~ 0
PIX_PWM_AUX4
Text GLabel 6400 3150 0    50   Input ~ 0
PIX_PWM_AUX6
Text GLabel 7350 4050 2    50   Input ~ 0
ESC_RPM_TAIL1
Text GLabel 7350 3950 2    50   Input ~ 0
ESC_RPM_TAIL2
Wire Wire Line
	7150 3950 7350 3950
Wire Wire Line
	7350 4050 7150 4050
Wire Wire Line
	7400 2000 7300 2000
Connection ~ 7300 2000
Wire Wire Line
	7300 2000 7300 2100
Text Notes 700  6750 0    50   Italic 0
Solder when needed
Text Notes 2700 4550 0    50   Italic 0
Solder when needed
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5DD3DB08
P 10100 4800
F 0 "#FLG0101" H 10100 4875 50  0001 C CNN
F 1 "PWR_FLAG" H 10100 4973 50  0000 C CNN
F 2 "" H 10100 4800 50  0001 C CNN
F 3 "~" H 10100 4800 50  0001 C CNN
	1    10100 4800
	1    0    0    -1  
$EndComp
Connection ~ 10100 4800
Wire Wire Line
	10100 4800 10200 4800
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5DD4F8E5
P 10100 4900
F 0 "#FLG0102" H 10100 4975 50  0001 C CNN
F 1 "PWR_FLAG" H 10100 5073 50  0000 C CNN
F 2 "" H 10100 4900 50  0001 C CNN
F 3 "~" H 10100 4900 50  0001 C CNN
	1    10100 4900
	-1   0    0    1   
$EndComp
Connection ~ 10100 4900
Wire Wire Line
	10100 4900 10200 4900
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 5DD583DF
P 10100 3950
F 0 "#FLG0103" H 10100 4025 50  0001 C CNN
F 1 "PWR_FLAG" H 10100 4123 50  0000 C CNN
F 2 "" H 10100 3950 50  0001 C CNN
F 3 "~" H 10100 3950 50  0001 C CNN
	1    10100 3950
	1    0    0    -1  
$EndComp
Connection ~ 10100 3950
Wire Wire Line
	10100 3950 10200 3950
Wire Wire Line
	4200 4600 4200 5850
Wire Wire Line
	4200 4600 5550 4600
Wire Wire Line
	3250 5850 4200 5850
Wire Wire Line
	4300 5950 4300 4700
Wire Wire Line
	4300 4700 5550 4700
Wire Wire Line
	3250 5950 4300 5950
$Comp
L Connector_Generic:Conn_01x04 J22
U 1 1 5DE5A815
P 3250 7000
F 0 "J22" H 3330 6992 50  0000 L CNN
F 1 "Goldpin_Male" H 3330 6901 50  0000 L CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.54mm" H 3250 7000 50  0001 C CNN
F 3 "~" H 3250 7000 50  0001 C CNN
	1    3250 7000
	-1   0    0    -1  
$EndComp
Text Notes 2650 7700 0    50   ~ 0
GPIO Connection
Wire Notes Line
	2650 7700 2650 6600
Wire Notes Line
	3650 7700 2650 7700
Wire Notes Line
	2650 6600 3650 6600
Text Notes 2600 7550 1    50   ~ 0
POS: insignificant
Text Notes 2700 6750 0    50   Italic 0
Solder when needed
Wire Notes Line
	3650 6600 3650 7700
Wire Wire Line
	3450 6900 4500 6900
Wire Wire Line
	4500 6900 4500 4900
Wire Wire Line
	4500 4900 5550 4900
Wire Wire Line
	5550 5000 4600 5000
Wire Wire Line
	4600 5000 4600 7000
Wire Wire Line
	4600 7000 3450 7000
Wire Wire Line
	3450 7100 4700 7100
Wire Wire Line
	4700 7100 4700 5100
Wire Wire Line
	4700 5100 5550 5100
Wire Wire Line
	5550 5200 4800 5200
Wire Wire Line
	4800 5200 4800 7200
Wire Wire Line
	4800 7200 3450 7200
$EndSCHEMATC
