#include "utility.h"
#include "globals.h"

#include "../../mavlink/include/archer/mavlink.h"
#include "parameters.h"

#include <string.h>

void handle_mavlink_char(uint8_t received_char, mavlink_message_t* received_msg) {
	if (mavlink_parse_char(MAVLINK_COMM_0, received_char, received_msg, &received_status)){
	    switch(received_msg->msgid){

    case MAVLINK_MSG_ID_PARAM_REQUEST_LIST:
		sent_msg_len = 0;
		for(uint16_t i=0; i<PARAM_COUNT; i++) {
			add_msg_param_value(sent_msg_buf, &sent_msg_len, i);
		}
		HAL_UART_Transmit(mavlink_huart, sent_msg_buf, sent_msg_len, 100);
		break;

	case MAVLINK_MSG_ID_PARAM_REQUEST_READ:
		sent_msg_len = 0;

		mavlink_param_request_read_t request_msg;
		mavlink_msg_param_request_read_decode(received_msg, &request_msg);
		if(request_msg.param_index == -1) { // Ignore index, search by id
			int16_t index = 0;
			param_t* param;
			findParam(request_msg.param_id, &index, &param);
			if(index != -1 && param != 0) {
				add_msg_param_value(sent_msg_buf, &sent_msg_len, index);
			}
		} else {
			if (request_msg.param_index >= 0 && request_msg.param_index < PARAM_COUNT) {
				add_msg_param_value(sent_msg_buf, &sent_msg_len, request_msg.param_index);
			}
		}

		if(sent_msg_len > 0) {
			HAL_UART_Transmit(mavlink_huart, sent_msg_buf, sent_msg_len, 100);
		}
		break;

	case MAVLINK_MSG_ID_PARAM_SET:
		sent_msg_len = 0;

		mavlink_param_set_t set_msg;
		mavlink_msg_param_set_decode(received_msg, &set_msg);
		int16_t index = 0;
		param_t* param;
		findParam(set_msg.param_id, &index, &param);
		if(index != -1 && param != 0) {
			if(param->type == set_msg.param_type) {
				memcpy(&(param->value.bytes), &(set_msg.param_value), sizeof(float));
			}

			add_msg_param_value(sent_msg_buf, &sent_msg_len, index);
		}
		if(sent_msg_len > 0) {
			HAL_UART_Transmit(mavlink_huart, sent_msg_buf, sent_msg_len, 100);
		}
		break;
	}
  }
}

void add_msg_param_value(uint8_t* buffer, uint16_t* offset, uint16_t paramIndex) {
	mavlink_message_t msg;
	mavlink_msg_param_value_pack(1, 158, &msg, parameters[paramIndex].id, parameters[paramIndex].value.param_float, parameters[paramIndex].type, PARAM_COUNT, paramIndex);
	*offset += mavlink_msg_to_send_buffer(buffer + *offset, &msg);
}

