#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <stdint.h>

#include "../../mavlink/include/archer/mavlink.h"

#define PARAM_ID_LEN 16

#define PARAM_COUNT 1

typedef struct Param {
	mavlink_param_union_t value;
	uint8_t type;
	char id[PARAM_ID_LEN];
} param_t;

param_t parameters[PARAM_COUNT];

void paramInit();

void findParam(char* id, int16_t* index, param_t** param);

param_t* paramGetById(char* id);

int16_t paramIndexGetById(char* id);

#endif //PARAMETERS_H
