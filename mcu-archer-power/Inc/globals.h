#ifndef GLOBALS_H
#define GLOBALS_H

#include "main.h"
#include "../../mavlink/include/archer/mavlink.h"

// ADC calibration

#define SLOPE_DIVISOR 1000

#define V_BATT_SLOPE 7035
#define V_BATT_OFFSET 338

#define I_AVIO_SLOPE 25
#define I_AVIO_OFFSET 3
// Servo channel does not work, value is always close to 1295 for any current passing through
// It is essentially disabled here, but handling is done normally to allow debugging with SEND_RAW
#define I_SERVO_SLOPE 0
#define I_SERVO_OFFSET 1

#define I_TAIL1_SLOPE 1712
#define I_TAIL1_OFFSET -593
#define I_TAIL2_SLOPE 1707
#define I_TAIL2_OFFSET -558
#define I_MAIN_SLOPE 6763
#define I_MAIN_OFFSET -2691

// MAVLink communication

UART_HandleTypeDef* mavlink_huart;

uint8_t received_char;
mavlink_message_t received_msg;
mavlink_status_t received_status;

mavlink_message_t sent_msg;
uint8_t sent_msg_buf[192];
uint16_t sent_msg_len;

// ARCHER_BATTERY handling

uint32_t adc_values[6];

uint32_t v_batt_value;
uint16_t v_batt_milivolts;
uint32_t i_avio_value;
uint16_t i_avio_centiamperes;
uint32_t i_servo_value;
uint16_t i_servo_centiamperes;
uint32_t i_tail1_value;
uint16_t i_tail1_centiamperes;
uint32_t i_tail2_value;
uint16_t i_tail2_centiamperes;
uint32_t i_main_value;
uint16_t i_main_centiamperes;

#endif //GLOBALS_H
