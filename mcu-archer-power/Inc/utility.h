#ifndef UTILITY_H
#define UTILITY_H

#include "../../mavlink/include/archer/mavlink.h"

void handle_mavlink_char(uint8_t received_char, mavlink_message_t* received_msg);

void add_msg_param_value(uint8_t* buffer, uint16_t* offset, uint16_t paramIndex);

#endif //UTILITY_H
