# archer-pcb

![ARCHER logo](archer_en_short.png)

Electrical design for **Autonomous Reconfigurable Compound Helicopter for Education and Research** project carried out on Faculty of Power and Aerospace Engineering of Warsaw University of Technology.

## Tools

- [KiCAD](http://www.kicad-pcb.org/)

## Contributors

- [Marek Łukasiewicz](http://lukasiewicz.tech/)
- Tobiasz Mandziuk - reviews

## TODO:

- Find out why thermometer measurement only rarely changes

### PCB fails:

- Oscillator is on wrong pins (LSE)
    - Those are for ~32kHz used for RTC
- Servo current channel does not work
- Avio current channel has 0 signal when powering 5V directly without a battery
- Programming IPC connector has pinout compatible with only some cheap ST-LINK knockoffs
- JST-XH is hard to disconnect, and it is not really important to have a goldpin connector, since every time a dedicated cable with JST-XH plug was used to ensure correct orientation
- I2C should have pullup resistors on both SCL and SDA
- Avio voltage regulator
    - OKY3502 has adjustable output voltage (which is good, since there is some drop between powerboard and autopilot), but the screw can be bumped into a different setting
    - A diode should probably be added ¿on high side of the regulator? to prevent voltage from 5V going into battery rail - more predictable, safer behavior when testing in lab
- Screws holding J3 in the panel should be more convenient to install, for example with some kind of threaded socket in the board

#### Fixed fails

- XT30PW-M on mainboard has pins assigned to wrong nets
    - Can be assembled on front side by flipping the plug upside down
- Didn't find resistors:
    - 3k3 - fixed by changing divider to 8k2 and 5k6
- 1-Wire is not pulled up to 3V3, parasitic mode should have signal delivered through a transistor to provide more power - fixed by gluing on another pin row
- Hall sensor reference voltage was 5V directly from voltage converter - this made current measurement slope dependent on voltage setting in the converter and avio rail load. Fixed by powering from a separate 5V voltage regulator

### Suggestions:

- Also have some 3V3 general-purpose pins
- MAV_MODE_FLAG_SAFETY_ARMED in standard HEARTBEAT message can be used to send if the motor jumper is plugged in

## Useful info:

- [Getting started with STM32F10xxx hardware development](https://www.st.com/content/ccc/resource/technical/document/application_note/6c/a3/24/49/a5/d4/4a/db/CD00164185.pdf/files/CD00164185.pdf/jcr:content/translations/en.CD00164185.pdf)
- [How to get the best ADC accuracy in STM32 microcontrollers](https://www.st.com/content/ccc/resource/technical/document/application_note/group0/3f/4c/a4/82/bd/63/4e/92/CD00211314/files/CD00211314.pdf/jcr:content/translations/en.CD00211314.pdf)
- [The Cube (pixhawk) interface specification](https://docs.cubepilot.org/user-guides/autopilot/the-cube-module-overview#the-cube-series-interface-specification)
    - Scroll a bit for pinouts
- [Via Currents and Temperatures](https://www.ultracad.com/articles/via.pdf) - article with actual measurements taken
- Mainboard space available:
    - Length (x): 80mm between fuselage braces
    - Width (y): 130mm inner fuselage size, 3mm module blocking holes every 15mm in two rows 99mm apart
    - Height (z): 24 mm tall side acces hole with 7.5mm corner radius

## Conventions

### Connectors

- Use female connector for power source whenever possible
    - Servomechanism are an exception (servos have female plug off the shelf)
- Use connectors that can only be plugged in one way
    - Servomechanisms are an exception, use colored goldpins to indicate pin functions
- Use AMASS XT family of connectors for high power connections (XT30-90, MR60 etc.)
- Use JST-GH and Clickmate connectors for Pixhawk connections
- Use goldpin connectors for other digital signals
    - Use JST-XH for UART connections
- Only connect one ground between two objects
    - Connect ground on highest power connector
    - Add a jumper between ground and ground pin on other connectors
- Assign pins to make straight cable connectors to COTS equipment
    - UART is an exception, all cables should cross over TX and RX
- Assign pins in this order when designing both ends of a cable connector
    - Power on pin 1 (and consecutive if needed)
    - Clock
    - Bidirectional data
    - Transmit
    - Receive
    - Ground on last pin (and preceding if needed)
- AMASS connectors are an exception, pin 1 is GND (usually, male horizontal XT30 was different)

### LEDs

- Red to indicate power is on
    - Orange and yellow can be used if there is need for more indicators close to each other
- Green as programmable LED
    - Steady on indicates correct operation
        - not really, does not show if the process is working if there is no transmission
    - Blinking indicates something is out of order
        - Faster blinking for more severe problems
        - ¿Single letter of Morse code if there is need for many messages?
- Blue for other indicators
    - e.g. TX and RX light (on when transmitting)
